# Java package for evolutionary art (Jpea)

## Download and Installation

1. [http://s1.saviola.de/misc/jpea_0.1.jar](Download Jpea JAR (~10 MB))
2. Place the JAR into its own directory
3. Run it! e.g., `java -jar jpea_0.1.jar`

## Usage

There are generally two different modes of operation:

### a) Interactive evaluation

At the scenario choice, choose "Interactive image evolution" to get to the interactive image evaluation. After that, you can change the JGAP configuration.

After submitting the configuration there will first be nothing. Depending on your CPU speed, it might take a minute or so until the first images are generated. There is currently no indication that something is happening before the images are displayed.

When the image creation is done, you will see up to 20 images. Now you can evaluate the images using the buttons to the right of the thumbnails. If you want more information on any image, left-click on it to show an enlarged version and information on the genotype. Clicking on the enlarged version again brings you back to the population overview.

At the bottom are three buttons: "Quit" will terminate the application, submit will submit all evaluations and generate the next generation of images and "Save image" (only active when looking at a single image) allows you to save the current image. It will be saved to the directory in which the JAR resides and have the name "saved_image.png". If you save another image like this, the last one will be overwritten–so copy it somewhere else first if you want to keep it!

### b) automatic evaluation

There are several scenario choices for automatic evaluation. First, there is fractal image creation. It uses fractals instead of arithemtic functions to create images, and GCF is used for evaluation. Then there is GCF evaluation, IC evaluation and a mixture of both, as well as several scenarios including bloat control (the genotype size will be taken into account when evaluating individuals, resulting in faster image creation speed in later stages–for arithmetic image creation).

After having chosen an automatic evaluation scenario, you can again change JGAP configuration parameters before the evolution starts. Once again, you will first see nothing for a while, it might take a little longer than for the interactive image evolution until something is displayed (also depending on the population size you chose in the JGAP configuration).

On the left side the best image of the current generation will be displayed, while on the right genotype information will be shown. In the status line at the bottom you will see information on the current generation.

The controls are slightly reduced, only offering options to quit and pause the evolution, both of which will only happen after the current image generation is done.

The best image of each generation is saved automatically in the same directory as the JAR file.

There is only one scenario for interactive evaluation, but multiple for automatic evaluation.

## Further reading

All of the information on the JGAP parameters as well as the automatic evaluation algorithms can be found in the thesis "Artificial Art: Image Generation using Evolutionary Algorithms", which can be downloaded on [saviola.de](http://saviola.de) if it was not part of this distribution.

Have fun!
