package de.saviola.jpea.core.create.function;

import org.apache.commons.math3.complex.Complex;

/**
 * An image function for fractals.
 * @param <V>
 */
public interface FractalImageFunction<V> extends ImageFunction<V>
{
  /**
   * Returns the background color value for the fractal.
   */
  public double getBackgroundValue();

  /**
   * Returns the starting point of the fractal (upper left corner).
   */
  public Complex getStartingPoint();

  /**
   * Returns the stepsize in both directions (zoom).
   */
  public double getStepSize();
}
