package de.saviola.jpea.core.evaluate.gui.individual;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JPanel;

import de.saviola.jpea.core.Individual;

/**
 * An evluation unit contains an {@link Individual} (i.e. its Phenotype) as
 * well as means to evaluate it.
 */
@SuppressWarnings("serial")
public class IndividualEvaluationUnit<G> extends IndividualThumbnail<G>
  implements
  ActionListener
{
  /**
   * Labels for the evaluation buttons.
   */
  private static String[] evaluationButtonLabels =
  { " --", " - ", " o ", " + ", " ++" };

  /**
   * Array of evaluation buttons.
   */
  private JButton[] evaluationButtons;

  /**
   * Basic evaluation values are between 0 and 4 (both included), this
   * multiplier can be used to increase the differences between the values.
   * 
   * E.g. a multiplier of 25 will result in a value stepping of
   * 
   * 0*25 = 0
   * 1*25 = 25
   * 2*25 = 50
   * 3*25 = 75
   * 4*25 = 100.
   * 
   * @see IndividualEvaluationUnit#setEvaluationMultiplier(int)
   * TODO make static?
   */
  private int evaluationMultiplier = 25;

  public IndividualEvaluationUnit(
    final Individual<G, BufferedImage, Double> individual,
    final IndividualViewer<G, BufferedImage, Double> viewer)
  {
    super(individual, viewer);

    this.initialize();
  }

  /**
   * When one of the evaluation button was clicked.
   */
  @Override
  public void actionPerformed(final ActionEvent e)
  {
    if (e.getSource() instanceof JButton)
    {
      final JButton button = (JButton) e.getSource();

      // Find the button index
      for (int i = 0; i < evaluationButtonLabels.length; i++)
      {
        if (button.getText().equals(evaluationButtonLabels[i]))
        {
          // And the set evaluation
          this.setEvaluation(i);

          return;
        }
      }
    }
  }

  /**
   * Returns the evaluation mutiplier.
   */
  public int getEvaluationMultiplier()
  {
    return this.evaluationMultiplier;
  }

  /**
   * Initializes the evaluation unit.
   */
  private void initialize()
  {
    // Setup button panel
    final JPanel buttonPanel = new JPanel(
      new GridLayout(evaluationButtonLabels.length, 1));

    this.evaluationButtons = new JButton[evaluationButtonLabels.length];

    // Create and add buttons
    for (int i = evaluationButtonLabels.length - 1; i >= 0; i--)
    {
      this.evaluationButtons[i] = new JButton(evaluationButtonLabels[i]);
      this.evaluationButtons[i].setSize(50, 30);
      buttonPanel.add(this.evaluationButtons[i]);
      this.evaluationButtons[i].addActionListener(this);
    }

    // Add button panel to main panel
    this.add(buttonPanel);

    // Set initial evaluation
    this.setEvaluation(this.getIndividual().hasEvaluation() ?
      this.getIndividual().getEvaluation().intValue()
        / this.evaluationMultiplier : 2);

    // TODO extract?
    this.setVisible(true);
  }

  /**
   * Enable or disable the evaluation buttons.
   */
  public void setButtonsEnabled(final boolean flag)
  {
    for (int i = 0; i < this.evaluationButtons.length; i++)
    {
      this.evaluationButtons[i].setEnabled(flag);
    }
  }

  /**
   * Sets the evaluation using the {@link #evaluationMultiplier}.
   * @param index
   */
  private void setEvaluation(final int index)
  {
    // Walk through all buttons and mark the chosen one red, all others blue
    for (int i = 0; i < evaluationButtonLabels.length; i++)
    {
      if (i == index)
      {
        // Multiply the index with the evaluation multiplier to get the actual
        // evaluation
        this.getIndividual()
          .setEvaluation(new Double(this.evaluationMultiplier * i));
        this.evaluationButtons[i].setForeground(Color.RED);
      }
      else
      {
        this.evaluationButtons[i].setForeground(Color.BLUE);
      }
    }
  }

  /**
   * Set the evaluation multiplier.
   */
  public void setEvaluationMultiplier(final int evaluationMultiplier)
  {
    this.evaluationMultiplier = evaluationMultiplier;
  }
}
