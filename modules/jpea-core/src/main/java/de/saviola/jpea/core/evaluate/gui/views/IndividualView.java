package de.saviola.jpea.core.evaluate.gui.views;

import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.gui.individual.CompositeIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.DefaultCompositeIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.DisplayableIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.IndividualViewer;

/**
 * The individual view in which individual viewers can be registered to show an
 * individual.
 */
public class IndividualView<G, V> extends JPanel implements
  CompositeIndividualViewer<G, BufferedImage, V>
{
  private static final long serialVersionUID = 7064493366067071644L;

  /**
   * Composite individual viewer.
   */
  private final DefaultCompositeIndividualViewer<G, BufferedImage, V> individualViewers;

  public IndividualView()
  {
    // TODO make this simpler by just expecting one (composite?) individual
    // viewer in the constructor and not implementing CompositeIndividualViewer
    this.individualViewers = new DefaultCompositeIndividualViewer<>();

    this.initialize();
  }

  /**
   * Adds an individual viewer to the interal composite individual viewer.
   * 
   * Check if it's a displayable individual viewer and add it to this panel if
   * it is.
   */
  @Override
  public void addIndividualViewer(
    final IndividualViewer<G, BufferedImage, V> viewer)
  {
    this.individualViewers.addIndividualViewer(viewer);

    if (viewer instanceof DisplayableIndividualViewer)
    {
      this.add(((DisplayableIndividualViewer<G, BufferedImage, V>) viewer)
        .getComponent());
    }
  }

  /**
   * Initialize this JPanel.
   */
  private void initialize()
  {
    // Center panel
    this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
  }

  @Override
  public void showIndividual(final Individual<G, BufferedImage, V> individual)
  {
    this.individualViewers.showIndividual(individual);
  }
}