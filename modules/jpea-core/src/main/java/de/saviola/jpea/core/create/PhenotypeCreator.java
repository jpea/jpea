package de.saviola.jpea.core.create;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;

/**
 * Interface for phenotype creators.
 * 
 * Phenotype creators use GP-programs or some other kind of source program to
 * create an image (or other form of representation of the genotype).
 * 
 * The type parameter G denotes the genotype class.
 * The type parameter P denotes the phenotype class.
 * 
 * TODO split into two interfaces like for evaluators.
 */
public interface PhenotypeCreator<G, P>
{
  /**
   * Creates the phenotype for the given individual.
   * 
   * Careful: If the phenotype could not be created (invalid genotype), the
   * individual will still have null as its phenotype.
   * 
   * TODO defect value besides null?
   */
  public void createForIndividual(Individual<G, P, ?> individual);

  /**
   * Create phenotypes for all individuals of a given population.
   * 
   * Default implementation is to call {@link #createForIndividual(Individual)}
   * for each individual of the population.
   */
  public void createForPopulation(Population<G, P, ?> population);
}
