package de.saviola.jpea.core.evaluate.gui.individual;

import java.awt.Component;

/**
 * Interface for displayable individual viewers which provide a method
 * {@link #getComponent()} which returns the component representing this viewer
 */
public interface DisplayableIndividualViewer<G, P, V> extends
  IndividualViewer<G, P, V>
{
  /**
   * Returns the component associated with this invidual viewer.
   */
  public Component getComponent();
}
