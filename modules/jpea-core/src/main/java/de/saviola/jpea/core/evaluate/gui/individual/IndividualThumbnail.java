package de.saviola.jpea.core.evaluate.gui.individual;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;

import org.imgscalr.Scalr;

import de.saviola.jpea.core.Individual;

/**
 * The thumbnail of an individual in the population view.
 */
public class IndividualThumbnail<G> extends JPanel implements MouseListener
{
  private static final long serialVersionUID = -6789092726434917261L;

  /**
   * The individual of this evaluation unit.
   */
  private final Individual<G, BufferedImage, Double> individual;

  /**
   * The individual viewer associated with this evaluation unit.
   * 
   * Will be used whenever the user clicks the image.
   */
  private final IndividualViewer<G, BufferedImage, Double> viewer;

  public IndividualThumbnail(
    final Individual<G, BufferedImage, Double> individual,
    final IndividualViewer<G, BufferedImage, Double> viewer)
  {
    super();

    this.individual = individual;
    this.viewer = viewer;

    this.initialize();
  }

  /**
   * Returns the individual associated with this thumbnail.
   */
  public Individual<G, BufferedImage, Double> getIndividual()
  {
    return this.individual;
  }

  /**
   * Returns the individual viewer of this thumbnail.
   * 
   * @see #viewer
   */
  public IndividualViewer<G, BufferedImage, Double> getViewer()
  {
    return this.viewer;
  }

  /**
   * Initializes the evaluation unit.
   */
  private void initialize()
  {
    // Use a downscaled version of the phenotype
    final BufferedImage image =
      Scalr.resize(this.individual.getPhenotype(), 150);

    // Setup the image panel
    final MarvinImagePanel imagePanel = new MarvinImagePanel();
    imagePanel.setImage(new MarvinImage(image));
    imagePanel.update();
    imagePanel.addMouseListener(this);
    imagePanel.setMinimumSize(new Dimension(150, 150));

    // Setup main panel
    this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    this.add(imagePanel);

    // TODO set this somewhere else?
    this.setVisible(true);
  }

  /**
   * When the thumbnail image was clicked, show the individual in the
   * individual viewer.
   */
  @Override
  public void mouseClicked(final MouseEvent arg0)
  {
    this.viewer.showIndividual(this.individual);
  }

  @Override
  public void mouseEntered(final MouseEvent arg0)
  {
    // nothing to do here
  }

  @Override
  public void mouseExited(final MouseEvent arg0)
  {
    // nothing to do here
  }

  @Override
  public void mousePressed(final MouseEvent arg0)
  {
    // nothint to do here

  }

  /**
   * To avoid small drags between pressing and releasing the mouse buttom from
   * preventing a click being registered, releasing the mouse will trigger a
   * click as well.
   */
  @Override
  public void mouseReleased(final MouseEvent arg0)
  {
    this.mouseClicked(arg0);
  }
}
