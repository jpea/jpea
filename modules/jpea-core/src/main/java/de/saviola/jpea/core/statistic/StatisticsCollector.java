package de.saviola.jpea.core.statistic;

import java.util.ArrayList;
import java.util.List;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.IndividualEvaluationStatistic;
import de.saviola.jpea.core.statistic.interfaces.PhenotypeCreationStatistic;
import de.saviola.jpea.core.statistic.interfaces.PopulationEvaluationStatistic;
import de.saviola.jpea.core.statistic.interfaces.RunStatistic;
import de.saviola.jpea.core.statistic.interfaces.Statistic;

/**
 * Statistics collector supporting all statistics interfaces.
 * 
 * Used to register statistical events, which will be passed on to all
 * registered statistics.
 * 
 * TODO avoid code repetition (reflection?)
 */
public class StatisticsCollector<G, P, V> implements RunStatistic<G, P, V>,
  PopulationEvaluationStatistic<G, P, V>, PhenotypeCreationStatistic<G, P, V>,
  IndividualEvaluationStatistic<G, P, V>
{
  /**
   * List of registered statistics.
   */
  private final List<Statistic<G, P, V>> statistics = new ArrayList<>();

  /**
   * Add a statistic.
   */
  public void addStatistic(final Statistic<G, P, V> statistic)
  {
    this.statistics.add(statistic);
  }

  @Override
  public void afterEvaluation(final Individual<G, P, V> individual,
    final V evaluation,
    final Object evaluatorId)
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof IndividualEvaluationStatistic)
      {
        ((IndividualEvaluationStatistic<G, P, V>) statistic)
          .afterEvaluation(individual, evaluation, evaluatorId);
      }
    }
  }

  @Override
  public void afterEvaluation(final Population<G, P, V> population)
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof PopulationEvaluationStatistic)
      {
        ((PopulationEvaluationStatistic<G, P, V>) statistic)
          .afterEvaluation(population);
      }
    }
  }

  @Override
  public void afterPhenotypeCreation(final Population<G, P, V> population)
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof PhenotypeCreationStatistic)
      {
        ((PhenotypeCreationStatistic<G, P, V>) statistic)
          .afterPhenotypeCreation(population);
      }
    }
  }

  @Override
  public void afterRunEnd()
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof RunStatistic)
      {
        ((RunStatistic<G, P, V>) statistic).afterRunEnd();
      }
    }
  }

  @Override
  public void beforeEvaluation(final Population<G, P, V> population)
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof PopulationEvaluationStatistic)
      {
        ((PopulationEvaluationStatistic<G, P, V>) statistic)
          .beforeEvaluation(population);
      }
    }
  }

  @Override
  public void beforePhenotypeCreation(final Population<G, P, V> population)
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof PhenotypeCreationStatistic)
      {
        ((PhenotypeCreationStatistic<G, P, V>) statistic)
          .beforePhenotypeCreation(population);
      }
    }
  }

  @Override
  public void beforeRunStart()
  {
    for (final Statistic<G, P, V> statistic : this.statistics)
    {
      if (statistic instanceof RunStatistic)
      {
        ((RunStatistic<G, P, V>) statistic).beforeRunStart();
      }
    }
  }
}