package de.saviola.jpea.core.evaluate.population;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;
import de.saviola.jpea.core.statistic.StatisticsCollector;

/**
 * Concurrent population evaluator.
 */
public class ConcurrentPopulationEvaluator<G, P, V> implements
  MultiObjectivePopulationEvaluator<G, P, V>
{
  private final static Logger logger = LoggerFactory
    .getLogger(ConcurrentPopulationEvaluator.class);

  /**
   * Evaluators used to evaluate the individuals.
   */
  private final List<IndividualEvaluator<G, P, V>> individualEvaluators =
    new ArrayList<>();

  /**
   * Pool size of the thread pool.
   */
  private final int poolSize;

  /**
   * Statistics collector.
   */
  private StatisticsCollector<G, P, V> statistics;

  public ConcurrentPopulationEvaluator()
  {
    // TODO DRY
    this.poolSize = Runtime.getRuntime().availableProcessors() * 2 + 1;
  }

  @Override
  public void addIndividualEvaluator(
    final IndividualEvaluator<G, P, V> evaluator)
  {
    this.individualEvaluators.add(evaluator);

  }

  /**
   * Evaluates the population concurrently, by spawning tasks for each
   * individual's evaluation.
   */
  @Override
  public void evaluatePopulation(final Population<G, P, V> population)
  {
    // Create a new thread pool
    final ExecutorService threadPool =
      Executors.newFixedThreadPool(this.poolSize);

    // Pre evaluation hook
    this.preEvaluation(population);

    // Walk through individuals and spawn a task for each
    for (final Individual<G, P, V> individual : population.getIndividuals())
    {
      threadPool.submit(new Callable<Individual<G, P, V>>()
      {
        /**
         * Evaluate individual with all individual evaluators.
         */
        @Override
        public Individual<G, P, V> call() throws Exception
        {
          for (int i = 0; i < ConcurrentPopulationEvaluator.this.individualEvaluators
            .size(); i++)
          {
            try
            {
              final V evaluation =
                ConcurrentPopulationEvaluator.this.individualEvaluators
                  .get(i).evaluateIndividual(individual);

              // Register the evaluation
              ConcurrentPopulationEvaluator.this
                .registerEvaluation(individual, evaluation, i);
            }
            catch (final NullPointerException eNull)
            {
              logger
                .debug("NullPointerException during evaluation, setting worst evaluation for individual.");

              individual.setEvaluation(
                ConcurrentPopulationEvaluator.this.individualEvaluators
                  .get(i).getWorstEvaluation());
            }
            catch (final RuntimeException e)
            {
              logger.warn("Error during evaluation: {}", e.getMessage());
              e.printStackTrace();
            }
          }

          return individual;
        }
      });
    }

    // Shutdown the thread pool and wait for its termination
    threadPool.shutdown();

    while (!threadPool.isTerminated())
    {
      try
      {
        // TODO reasonable timeout?
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
      }
      catch (final InterruptedException e)
      {
        // TODO Handle exception
        e.printStackTrace();
      }
    }

    this.postEvaluation(population);
  }

  /**
   * Returns the first individual evaluator.
   */
  @Override
  public IndividualEvaluator<G, P, V> getIndividualEvaluator()
  {
    return this.getIndividualEvaluator(0);
  }

  @Override
  public IndividualEvaluator<G, P, V> getIndividualEvaluator(final int id)
  {
    return this.individualEvaluators.get(id);
  }

  @Override
  public List<IndividualEvaluator<G, P, V>> getIndividualEvaluators()
  {
    return this.individualEvaluators;
  }

  @Override
  public StatisticsCollector<G, P, V> getStatisticsCollector()
  {
    return this.statistics;
  }

  @Override
  public void postEvaluation(final Population<G, P, V> population)
  {
    // Nothing to do here, may be overridden by subclasses
  }

  @Override
  public void preEvaluation(final Population<G, P, V> population)
  {
    // Nothing to do here, may be overridden by subclasses
  }

  @Override
  public synchronized void registerEvaluation(
    final Individual<G, P, V> individual,
    final V evaluation, final int evaluatorId)
  {
    // Registers the evaluation with the statistics collector, if one was set
    if (this.statistics != null)
    {
      this.statistics.afterEvaluation(individual, evaluation, new Integer(
        evaluatorId));
    }
  }

  @Override
  public void setIndividualEvaluator(
    final IndividualEvaluator<G, P, V> individualEvaluator)
  {
    this.individualEvaluators.clear();
    this.individualEvaluators.add(individualEvaluator);
  }

  @Override
  public void setStatisticsCollector(
    final StatisticsCollector<G, P, V> statistics)
  {
    this.statistics = statistics;
  }
}
