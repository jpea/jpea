package de.saviola.jpea.core.application;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * GUI Configurator that acts as a composite for multiple other configurators.
 * 
 * It supports non-GUI configurators as well, but usually it will show one
 * frame and expect configurators to be a component which will then be added
 * to the frame (page axis box layout).
 */
@SuppressWarnings("serial")
public class CompositeGuiConfigurator extends JFrame implements Configurator,
  ActionListener
{
  /**
   * Lock, condition and flag used to wait for the configuration to be done.
   */
  private final Lock lock = new ReentrantLock();
  private final Condition conditionDone = this.lock.newCondition();
  private boolean done = false;

  /**
   * List of configurators.
   */
  private final List<Configurator> configurators = new ArrayList<>();

  /**
   * GUI elements, namely the main panel and submit button.
   */
  private JPanel panel;
  private JButton buttonSubmit;

  /**
   * When the submit button is clicked, the configuration of all configurators
   * is updated.
   */
  @Override
  public void actionPerformed(final ActionEvent event)
  {
    // If the submit button was clicked
    if (event.getSource().equals(this.buttonSubmit))
    {
      // Try to configure all configurators.
      try
      {
        for (final Configurator configurator : this.configurators)
        {
          configurator.updateConfiguration();
        }
      }
      // If something went wrong, we assume it's about the user's choices
      // TODO more specific error messages
      catch (final Exception e)
      {
        JOptionPane.showMessageDialog(this,
          "Configuration incomplete or invalid, please choose valid options.");

        e.printStackTrace();

        return;
      }

      // Signal the completion of the configuration process to threads waiting
      // in updateConfiguration
      this.lock.lock();
      this.done = true;
      this.conditionDone.signalAll();
      this.lock.unlock();

      // Hide and dispose of the configurator
      this.setVisible(false);
      this.dispose();

      System.out.println("Configurator disposed of.");
    }
  }

  /**
   * Adds a configurator to the list.
   * 
   * Won't let you add itself to the list.
   * 
   * TODO don't allow adding things after {@link #initialize()} has been called
   */
  public void addConfigurator(final Configurator configurator)
  {
    // Will not add itself
    if (!configurator.equals(this))
    {
      this.configurators.add(configurator);
    }
  }

  /**
   * Will initialize the configurator.
   * 
   * This will initialize each configurator contained and setup the frame.
   */
  @Override
  public void initialize()
  {
    // Abort if we're not on the EDT
    if (!SwingUtilities.isEventDispatchThread())
    {
      throw new RuntimeException("GUI modification must happen on the EDT!");
    }

    // If no configurators were added
    // TODO allow this and skip configuration in this case?
    if (this.configurators.isEmpty())
    {
      throw new RuntimeException("Add all configurators prior to calling " +
        "initialize().");
    }

    // Setup main panel
    this.panel = new JPanel();
    this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.PAGE_AXIS));
    this.panel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

    // Initialize and add configurators to the panel which extend Component
    for (final Configurator configurator : this.configurators)
    {
      if (configurator instanceof Component)
      {
        this.panel.add((Component) configurator);
      }

      configurator.initialize();
    }

    // Setup submit button
    this.buttonSubmit = new JButton("Submit");
    this.buttonSubmit.addActionListener(this);
    this.panel.add(this.buttonSubmit);

    this.getContentPane().add(this.panel);

    // Setup frame
    this.pack();
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setVisible(true);
  }

  /**
   * Waits for the configuration of all contained configurators.
   * 
   * Will only return when the configuration is complete.
   */
  @Override
  public void updateConfiguration()
  {
    this.done = false;

    try
    {
      this.lock.lock();

      while (!this.done)
      {
        this.conditionDone.await();
      }
    }
    catch (final InterruptedException e)
    {
      e.printStackTrace();
    }
    finally
    {
      this.lock.unlock();
    }
  }

}
