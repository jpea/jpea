package de.saviola.jpea.core.evaluate;

/**
 * Interface for fitness evaluators.
 * 
 * Fitness evaluators can compare two evaluations and tell which one is better.
 */
public interface FitnessEvaluator<V>
{
  public boolean isFitter(V d1, V d2);
}
