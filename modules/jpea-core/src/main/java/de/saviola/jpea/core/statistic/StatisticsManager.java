package de.saviola.jpea.core.statistic;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brsanthu.dataexporter.DataExporter;
import com.brsanthu.dataexporter.output.csv.CsvExporter;

/**
 * Singleton implementation of a statistics manager.
 */
public class StatisticsManager
{
  /**
   * Manager instance.
   */
  private static StatisticsManager instance;

  private final static Logger logger = LoggerFactory
    .getLogger(StatisticsManager.class);

  /**
   * Returns the statistics manager.
   */
  public static StatisticsManager getInstance()
  {
    if (instance == null)
    {
      instance = new StatisticsManager();
    }

    return instance;
  }

  /**
   * List of created exporters.
   */
  private final Collection<DataExporter> exporters = new ArrayList<>();

  /**
   * Output directory for statistics.
   */
  private String outputDirectory = "./";

  /**
   * Timestamp, uniquely identifying statistics.
   */
  private final long timestamp;

  private StatisticsManager()
  {
    this.timestamp = new Date().getTime();
  }

  /**
   * Create new data exporter with the given ID.
   * 
   * TODO create directories?
   */
  public DataExporter createDataExporter(final String id)
  {
    Writer out;
    final String fileName = this.outputDirectory + this.timestamp + "_" + id +
      ".csv";

    // Try to create the file for the exporter
    try
    {
      out = new PrintWriter(fileName);
    }
    catch (final FileNotFoundException e)
    {
      logger.error("Could not create file \"{}\" for statistic output, will " +
        "write to console instead.", fileName);

      out = new PrintWriter(System.out, false);
    }

    final DataExporter dataExporter = new CsvExporter(out);

    this.exporters.add(dataExporter);

    return dataExporter;
  }

  /**
   * Will finalize statistics by flushing them (if necessary), writing table
   * footers etc.
   * 
   * Should be called when all data has been collected, additional data will
   * not be accepted afterwards.
   */
  public void finalizeStatistics()
  {
    for (final DataExporter exporter : this.exporters)
    {
      try
      {
        exporter.finishExporting();
      }
      catch (final IllegalStateException e)
      {
        logger.warn("DataExporter has not been started, nothing will be " +
          "written.");
      }
    }
  }

  /**
   * Sets a new output directory.
   * 
   * Will only be used for newly created data exporters.
   */
  public void setOutputDirectory(String directory)
  {
    // Append slash if necessary
    if (!directory.endsWith("/"))
    {
      directory = directory + "/";
    }

    this.outputDirectory = directory;
  }
}
