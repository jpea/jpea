package de.saviola.jpea.core.application;

/**
 * Interface for configurators, consisting of two methods for initialization
 * and the actual configuration process.
 */
public interface Configurator
{
  /**
   * Initialize this configurator.
   */
  public void initialize();

  /**
   * Update the configuration.
   * 
   * TODO rename? configure() or something?
   */
  public void updateConfiguration() throws Exception;
}
