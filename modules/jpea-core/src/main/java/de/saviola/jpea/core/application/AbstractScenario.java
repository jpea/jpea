package de.saviola.jpea.core.application;

/**
 * Represents an abstract scenario, consisting of a {@link FrameworkDriver} and
 * and {@link EvolutionEngine}.
 * 
 * Contains the default {@link #start()} implementation, which just calls
 * {@link FrameworkDriver#startEvolution(EvolutionEngine)}.
 * 
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation type of the individuals.
 */
public abstract class AbstractScenario<G, P, V> implements Scenario
{
  /**
   * The framework driver used by this scenario.
   */
  private FrameworkDriver<G, P, V> driver;

  /**
   * The evolution engine used by this scenario.
   */
  private EvolutionEngine<G, P, V> evolutionEngine;

  /**
   * Returns the framework driver.
   */
  public FrameworkDriver<G, P, V> getFrameworkDriver()
  {
    return this.driver;
  }

  /**
   * Returns the evolution engine.
   */
  public EvolutionEngine<G, P, V> getEvolutionEngine()
  {
    return this.evolutionEngine;
  }

  /**
   * Sets the framework driver.
   */
  public void setFrameworkDriver(FrameworkDriver<G, P, V> driver)
  {
    this.driver = driver;
  }

  /**
   * Sets the evolution engine.
   */
  public void setEvolutionEngine(EvolutionEngine<G, P, V> evolutionEngine)
  {
    this.evolutionEngine = evolutionEngine;
  }

  /**
   * Starts the evolution by calling
   * {@link FrameworkDriver#startEvolution(EvolutionEngine)}.
   */
  @Override
  public void start()
  {
    try
    {
      this.driver.startEvolution(this.evolutionEngine);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

}
