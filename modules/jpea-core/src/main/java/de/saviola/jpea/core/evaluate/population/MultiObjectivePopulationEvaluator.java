package de.saviola.jpea.core.evaluate.population;

import java.util.List;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;

/**
 * Interface for multi-objective population evaluators.
 */
public interface MultiObjectivePopulationEvaluator<G, P, V> extends
  PopulationEvaluator<G, P, V>
{
  /**
   * Add an individual evaluator.
   */
  public void addIndividualEvaluator(
    final IndividualEvaluator<G, P, V> evaluator);

  /**
   * Returns the individual evaluator with the given ID.
   */
  public IndividualEvaluator<G, P, V> getIndividualEvaluator(final int id);

  /**
   * Returns the individual evaluators.
   */
  public List<IndividualEvaluator<G, P, V>> getIndividualEvaluators();

  /**
   * Post-evaluation hook.
   */
  public void postEvaluation(Population<G, P, V> population);

  /**
   * Pre-evaluation hook.
   */
  public void preEvaluation(Population<G, P, V> population);

  /**
   * Registers an evaluation by the evaluator identified by the given ID for
   * the given individual.
   */
  public void registerEvaluation(final Individual<G, P, V> individual,
    final V evaluation, final int evaluatorId);
}
