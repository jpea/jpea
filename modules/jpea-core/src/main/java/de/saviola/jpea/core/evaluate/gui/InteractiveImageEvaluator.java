package de.saviola.jpea.core.evaluate.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.evaluate.PhenotypeDatabase;
import de.saviola.jpea.core.evaluate.gui.individual.CompositeIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.EnlargedIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.IndividualEvaluationUnit;
import de.saviola.jpea.core.evaluate.gui.individual.IndividualViewer;
import de.saviola.jpea.core.evaluate.gui.views.IndividualView;
import de.saviola.jpea.core.evaluate.gui.views.PopulationView;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.exception.EndOfEvolutionException;
import de.saviola.jpea.core.statistic.StatisticsCollector;

/**
 * GUI evaluator that implements a user-directed evolution by allowing the user
 * to evaluate the generated images with a value between 0 (asthetically bad)
 * and 100 (asthetically good).
 * 
 * @param <G> genotype of the evaluated individuals.
 */
@SuppressWarnings("serial")
public class InteractiveImageEvaluator<G> extends EvaluatorGui implements
  PopulationEvaluator<G, BufferedImage, Double>,
  CompositeIndividualViewer<G, BufferedImage, Double>,
  IndividualEvaluator<G, BufferedImage, Double>
{
  private static final Logger logger = LoggerFactory
    .getLogger(InteractiveImageEvaluator.class);

  /**
   * Buttons of the GUI.
   */
  private JButton buttonNext, buttonSave;
  /**
   * The individual that is currently being shown in the individual viewers.
   */
  private Individual<G, BufferedImage, Double> currentIndividual;

  /**
   * The population that's currently evaluated.
   */
  private Population<G, BufferedImage, Double> currentPopulation;

  /**
   * State variables for the above locking conditions.
   */
  private boolean evaluated;

  /**
   * Evaluation units for the individuals.
   */
  private final List<IndividualEvaluationUnit<G>> evaluationUnits =
    new ArrayList<>();

  /**
   * The enlarged individual viewer.
   */
  private EnlargedIndividualViewer<G, Double> imagePanel;

  /**
   * Panels of the GUI.
   */
  private IndividualView<G, Double> individualView;

  /**
   * This lock and its conditions are used to synchronize the gui thread with
   * the evolution thread.
   */
  public final Lock lock = new ReentrantLock();
  public final Condition lockEvaluationCondition = this.lock.newCondition();

  /**
   * Phenotype database.
   */
  private final PhenotypeDatabase<BufferedImage, Double> phenotypeDatabase =
    new PhenotypeDatabase<>(30, false);

  private PopulationView populationView;

  /**
   * If true, the application is supposed to quit asap.
   */
  private boolean quit = false;

  public InteractiveImageEvaluator()
  {
    super();

    this.initialize();
  }

  /**
   * Will be called when one of the buttons is pressed.
   */
  @Override
  public void actionPerformed(final ActionEvent event)
  {
    // Save current image
    if (event.getSource().equals(this.buttonSave)
      && this.currentIndividual != null)
    {
      MarvinImageIO.saveImage(
        new MarvinImage(this.currentIndividual.getPhenotype()),
        "saved_image.png");

      logger.debug("Current image saved to \"saved_image.png\".");
    }
    // Submit current evaluations
    else if (event.getSource().equals(this.buttonNext))
    {
      this.disableEvaluationButtons();
      this.storeEvaluations();
      this.buttonNext.setText("Generating images…");
      this.buttonNext.setEnabled(false);

      this.notifyEvaluationComplete();
    }
    // Or just quit
    else if (event.getSource().equals(this.getButtonQuit()))
    {
      this.quit = true;

      this.notifyEvaluationComplete();
    }
  }

  /**
   * Adds the given individual to the GUI by displaying its thumbnail.
   */
  private void addIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // Set the current individual if none was set before
    if (this.currentIndividual == null && individual.getPhenotype() != null)
    {
      this.currentIndividual = individual;
    }

    // Individuals without phenotype are not displayed and receive worst
    // evaluation
    if (individual.getPhenotype() == null)
    {
      individual.setEvaluation(new Double(0));

      logger.debug("Encountered individual without phenotype.");

      return;
    }

    // Check our phenotype database for the image
    final BufferedImage imageCopy = Scalr.resize(individual.getPhenotype(), 50);

    // TODO extract default evaluation
    individual.setEvaluation(this.phenotypeDatabase
      .getEvaluation(imageCopy, new Double(50)));

    this.evaluationUnits.add(new IndividualEvaluationUnit<>(individual, this));
  }

  /**
   * Adds a new individual viewer.
   */
  @Override
  public void addIndividualViewer(
    final IndividualViewer<G, BufferedImage, Double> viewer)
  {
    this.individualView.addIndividualViewer(viewer);
  }

  /**
   * Disable evaluation buttons on all evaluation units.
   */
  private void disableEvaluationButtons()
  {
    for (final Component c : this.populationView.getComponents())
    {
      ((IndividualEvaluationUnit<?>) c).setButtonsEnabled(false);
    }
  }

  /**
   * This operation is not supported as the individuals are evaluated by the
   * user (and not one by one).
   */
  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    throw new UnsupportedOperationException();
  }

  /**
   * Used to display all images of the population to the user as thumbnails.
   */
  @Override
  public void evaluatePopulation(
    final Population<G, BufferedImage, Double> population)
  {
    // Reset the evaluation state
    this.evaluated = false;

    // Update current population
    this.currentPopulation = population;
    // Clear evaluation units
    this.evaluationUnits.clear();

    for (final Individual<G, BufferedImage, Double> individual : population
      .getIndividuals())
    {
      this.addIndividual(individual);
    }

    // And replace the thumbnails
    SwingUtilities.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        InteractiveImageEvaluator.this
          .replaceThumbnails(InteractiveImageEvaluator.this.evaluationUnits);
      }
    });

    // Now we await the user evaluation
    try
    {
      this.lock.lock();

      while (!this.evaluated)
      {
        logger.debug("Awaiting user evaluation…");
        this.lockEvaluationCondition.await();
      }
    }
    catch (final InterruptedException e)
    {
      // TODO
      e.printStackTrace();
    }
    finally
    {
      this.lock.unlock();
    }

    if (this.quit)
    {
      this.dispose();

      throw new EndOfEvolutionException();
    }
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(100.);
  }

  @Override
  public Comparator<Double> getComparator()
  {
    return new DynamicDoubleComparator<>(100.);
  }

  @Override
  public IndividualEvaluator<G, BufferedImage, Double> getIndividualEvaluator()
  {
    return this;
  }

  @Override
  public StatisticsCollector<G, BufferedImage, Double> getStatisticsCollector()
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(0.);
  }

  /**
   * Initialized the GUI.
   * 
   * @see #run()
   */
  private void initialize()
  {
    this.phenotypeDatabase.setComparator(new ImageComparator());

    this.populationView = new PopulationView();

    // Buttons
    this.buttonNext = new JButton("Generating images…");
    this.buttonNext.setEnabled(false);
    this.buttonNext.addActionListener(this);

    // TODO move to individual view
    this.buttonSave = new JButton("Save image");
    this.buttonSave.addActionListener(this);

    this.getControlPanel().add(this.buttonNext);
    this.getControlPanel().add(this.buttonSave);

    this.individualView = new IndividualView<>();

    // ImagePanel
    this.imagePanel = new EnlargedIndividualViewer<>();
    this.addIndividualViewer(this.imagePanel);
    // Click on enlarged individual returns the focus to the population
    // TODO extract
    ((JPanel) this.imagePanel.getComponent()).addMouseListener(
      new MouseAdapter()
      {
        @Override
        public void mouseClicked(final MouseEvent arg0)
        {
          InteractiveImageEvaluator.this.showPopulationView();
        }

        @Override
        public void mouseReleased(final MouseEvent arg0)
        {
          this.mouseClicked(arg0);
        }
      });

    // Add panels
    this.getViews().addTab("Population", this.populationView);
    this.getViews().addTab("Individual", this.individualView);

    this.showGui();
  }

  /**
   * Notifies waiting threads that the evaluation of the current generation is
   * done.
   */
  private void notifyEvaluationComplete()
  {
    // Notify other threads that the image evaluation is done
    try
    {
      this.lock.lock();

      this.evaluated = true;
      this.lockEvaluationCondition.signalAll();
    }
    finally
    {
      this.lock.unlock();
    }
  }

  /**
   * Replace the thumbnails.
   * 
   * TODO rename?
   */
  private void replaceThumbnails(
    final List<IndividualEvaluationUnit<G>> evaluationUnits)
  {
    // Remove current thumbnails
    this.populationView.replaceThumbnails(evaluationUnits);

    this.buttonNext.setText("Submit");
    this.buttonNext.setEnabled(true);
  }

  /**
   * Not supported as there is only one individual evaluator: The user.
   */
  @Override
  public void setIndividualEvaluator(
    final IndividualEvaluator<G, BufferedImage, Double> individualEvaluator)
  {
    throw new UnsupportedOperationException();
  }

  /**
   * Statistics collector is not supported for interactive evolution.
   * 
   * TODO throw exception?
   */
  @Override
  public void setStatisticsCollector(
    final StatisticsCollector<G, BufferedImage, Double> statistics)
  {
    // Do not support statistics collector

  }

  /**
   * Show the given individual in all individual viewers.
   */
  @Override
  public void showIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    this.currentIndividual = individual;

    this.individualView.showIndividual(individual);

    this.showIndividualViewers();

    //this.pack();
  }

  /**
   * Switch to the individual view.
   * 
   * TODO rename? sowIndividualView()?
   */
  private void showIndividualViewers()
  {
    this.buttonSave.setEnabled(true);
    this.buttonNext.setEnabled(false);

    this.getViews().setSelectedComponent(this.individualView);
  }

  /**
   * Switch to the population view.
   */
  private void showPopulationView()
  {
    this.buttonSave.setEnabled(false);
    this.buttonNext.setEnabled(true);

    this.getViews().setSelectedComponent(this.populationView);
  }

  /**
   * Stores the evaluations of the current population in the database.
   */
  private void storeEvaluations()
  {
    BufferedImage imageCopy;

    for (final Individual<G, BufferedImage, Double> individual : this.currentPopulation
      .getIndividuals())
    {
      if (individual.getPhenotype() == null)
      {
        continue;
      }

      imageCopy = Scalr.resize(individual.getPhenotype(), 50);

      this.phenotypeDatabase.putEvaluation(imageCopy,
        individual.getEvaluation());
    }
  }
}