package de.saviola.jpea.core.statistic.interfaces;

import de.saviola.jpea.core.Population;

/**
 * Interface for population evaluation statistics.
 */
public interface PopulationEvaluationStatistic<G, P, V> extends
  Statistic<G, P, V>
{
  /**
   * To be called after the given population has been evaluated.
   */
  public void afterEvaluation(Population<G, P, V> population);

  /**
   * To be called before the given population is evaluated.
   */
  public void beforeEvaluation(Population<G, P, V> population);
}
