package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Image complexity evaluator.
 * 
 * Image complexity is measured by compressed size and RMSE of the compressed
 * image compared to the original (JPEG compression).
 * 
 * TODO add optimization goal
 */
public class ImageComplexityEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  public ImageComplexityEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Calculates the image complexity of the phenotype of the given individual.
   */
  private Double calculateImageComplexity(
    final Individual<G, BufferedImage, Double> individual)
  {
    final BufferedImage bufferedImage =
      individual.getPhenotype();
    BufferedImage jpegImage = null;

    final ByteArrayOutputStream os = new ByteArrayOutputStream();
    final ImageOutputStream stream =
      new MemoryCacheImageOutputStream(os);
    final int sizeOriginal = 500 * 500;
    int sizeCompressed = 500 * 500;

    // Write the compressed image to a memory cache stream
    try
    {
      ImageIO.write(bufferedImage, "jpg", stream);
      stream.flush();
      final byte[] jpgByteArray = os.toByteArray();
      sizeCompressed = jpgByteArray.length;
      final ByteArrayInputStream bis =
        new ByteArrayInputStream(jpgByteArray);
      jpegImage = ImageIO.read(bis);
    }
    catch (final IOException e)
    {
      // TODO handle
      e.printStackTrace();
    }
    finally
    {
      // TODO extract or handle cleaner
      try
      {
        stream.close();
      }
      catch (final IOException e)
      {
        e.printStackTrace();
      }
    }

    // If the image could not be created, return 0 (worst image complexity)
    if (jpegImage == null)
    {
      return this.getWorstEvaluation();
    }

    // Delegate image comparison to RmseEvaluator
    final RmseEvaluator<G> rmseEvaluator = new RmseEvaluator<>(jpegImage);

    final double rms =
      rmseEvaluator.evaluateIndividual(individual).doubleValue();

    return new Double(rms / (sizeOriginal / (double) sizeCompressed));
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // No phenotype means worst evaluation
    if (!individual.hasPhenotype())
    {
      return this.getWorstEvaluation();
    }

    return this.calculateImageComplexity(individual);
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(0.);
  }

}
