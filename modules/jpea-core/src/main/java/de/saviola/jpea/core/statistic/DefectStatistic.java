package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.PopulationEvaluationStatistic;

public class DefectStatistic<G, P, V> extends GenerationStatistic implements
  PopulationEvaluationStatistic<G, P, V>
{

  @Override
  public void afterEvaluation(final Population<G, P, V> population)
  {
    int defectCounter = 0;

    for (final Individual<G, P, V> individual : population.getIndividuals())
    {
      if (!individual.hasPhenotype())
      {
        defectCounter++;
      }
    }

    this.addRow(new Integer(this.getGeneration()), new Integer(defectCounter));
  }

  @Override
  public void beforeEvaluation(final Population<G, P, V> population)
  {
    this.incrementGeneration();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "generation", "defect individuals" };
  }

}
