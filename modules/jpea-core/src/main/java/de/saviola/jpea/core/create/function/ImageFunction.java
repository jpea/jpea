package de.saviola.jpea.core.create.function;

/**
 * Interface representing an image function.
 * 
 * It can be evaluated for arbitrary x and y coordinates.
 * 
 * @param <V> denotes the return type of the image function.
 */
public interface ImageFunction<V>
{
  /**
   * Returns the result of the evalutation of the image function at the given
   * x and y parameter values.
   */
  public V evaluateAt(double x, double y);
}
