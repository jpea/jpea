package de.saviola.jpea.core.create;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;

/**
 * Abstract phenotype creator, implementing only the
 * {@link #createForPopulation(Population)} method.
 * 
 * @param <G> Genotype from which the phenotype is creatd
 * @param <P> Phenotype which is created
 */
public abstract class AbstractPhenotypeCreator<G, P>
  implements PhenotypeCreator<G, P>
{
  /**
   * Default implementation of the {@link #createForPopulation(Population)}
   * method, calling {@link #createForIndividual(Individual)} for each
   * individual of the population.
   */
  @Override
  public void createForPopulation(Population<G, P, ?> population)
  {
    for (Individual<G, P, ?> individual : population.getIndividuals())
    {
      this.createForIndividual(individual);
    }
  }
}
