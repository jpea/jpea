package de.saviola.jpea.core.application;

/**
 * Interface for a scenario, containing mainly desciptive methods and
 * {@link #start()}.
 */
public interface Scenario
{
  /**
   * Longer description of the scenario, currently unused.
   */
  public String getDescription();

  /**
   * Short title for the scenario.
   */
  public String getTitle();

  /**
   * Start the scenario.
   */
  public void start() throws Exception;
}
