package de.saviola.jpea.core.application;

import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.statistic.StatisticsCollector;

/**
 * An evolution engine encapsulates a {@link PhenotypeCreator} and an
 * {@link IndividualEvaluator} and makes sure that phenotypes for individuals are created
 * prior to evaluation.
 * 
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation type of the individuals.
 * 
 * TODO better name.
 */
public class EvolutionEngine<G, P, V>
{
  /**
   * The evaluator.
   */
  private final PopulationEvaluator<G, P, V> evaluator;

  /**
   * The phenotype creator.
   */
  private final PhenotypeCreator<G, P> phenotypeCreator;

  /**
   * Statistics collector.
   */
  private final StatisticsCollector<G, P, V> statistics;

  public EvolutionEngine(final PhenotypeCreator<G, P> phenotypeCreator,
    final PopulationEvaluator<G, P, V> evaluator,
    final StatisticsCollector<G, P, V> statistics)
  {
    this.evaluator = evaluator;
    this.phenotypeCreator = phenotypeCreator;
    this.statistics = statistics;
  }

  /**
   * Processes a population, first creating phenotype for all individuals and
   * then evaluating all of them.
   */
  public void processPopulation(final Population<G, P, V> population)
  {
    this.statistics.beforePhenotypeCreation(population);
    this.phenotypeCreator.createForPopulation(population);
    this.statistics.afterPhenotypeCreation(population);

    this.statistics.beforeEvaluation(population);
    this.evaluator.evaluatePopulation(population);
    this.statistics.afterEvaluation(population);
  }
}
