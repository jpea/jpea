package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Root-mean-square error (RMSE) evaluator, comparing two images.
 */
public class RmseEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  /**
   * The target image.
   */
  private final BufferedImage targetImage;

  public RmseEvaluator(final BufferedImage targetImage)
  {
    this.targetImage = targetImage;
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Calculate RMSE between the two given images.
   */
  public double calculateRmse(final BufferedImage image1,
    final BufferedImage image2)
  {
    // TODO make sure both images have the same size
    final int width = image1.getWidth(), height =
      image1.getHeight();
    final int[] colorArray1 =
      ((DataBufferInt) image1.getRaster().getDataBuffer()).getData();
    final int[] colorArray2 =
      ((DataBufferInt) image2.getRaster().getDataBuffer()).getData();

    int r, g, b, rgbOrig, rgbCompressed;
    double error = 0.;

    for (int i = 0; i < height; i++)
    {
      for (int j = 0; j < width; j++)
      {
        rgbOrig = colorArray1[i * width + j];
        rgbCompressed = colorArray2[i * width + j];

        r = ((rgbOrig & 0x00FF0000) >>> 16)
          - ((rgbCompressed & 0x00FF0000) >>> 16);
        g = ((rgbOrig & 0x0000FF00) >>> 8)
          - ((rgbCompressed & 0x0000FF00) >>> 8);
        b = (rgbOrig & 0x000000FF)
          - (rgbCompressed & 0x000000FF);

        error += r * r + g * g + b * b;
      }
    }

    return Math.sqrt(error);
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // No phenotype means worst evaluation
    if (!individual.hasPhenotype())
    {
      return this.getWorstEvaluation();
    }

    return new Double(this.calculateRmse(individual.getPhenotype(),
      this.targetImage));
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(0.);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }

}
