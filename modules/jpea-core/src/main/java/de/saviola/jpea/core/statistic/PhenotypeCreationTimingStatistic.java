package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.PhenotypeCreationStatistic;

/**
 * Phenotype creation timing statistic.
 * 
 * TODO naming (decorator pattern?)
 */
public class PhenotypeCreationTimingStatistic<G, P, V> extends
  TimingStatistic implements PhenotypeCreationStatistic<G, P, V>
{
  @Override
  public void afterPhenotypeCreation(final Population<G, P, V> population)
  {
    this.stop();
  }

  @Override
  public void beforePhenotypeCreation(final Population<G, P, V> population)
  {
    this.incrementGeneration();
    this.start(new Integer(this.getGeneration()));

  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "generation", "phenotype creation time" };
  }
}
