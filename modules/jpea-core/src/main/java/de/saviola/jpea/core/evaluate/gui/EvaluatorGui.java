package de.saviola.jpea.core.evaluate.gui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * A general GUI for evaluators, offering multiple views and management of
 * control buttons.
 */
public abstract class EvaluatorGui extends JFrame implements ActionListener
{
  private static final long serialVersionUID = -9172172384971280968L;

  /**
   * The quit button. Every GUI needs one of these.
   */
  private JButton buttonQuit;

  /**
   * The bottom panels.
   */
  private JPanel panelControl, panelStatus;

  /**
   * Label for the status panel.
   */
  private JLabel status;

  /**
   * The views, organized in a tabbed pane.
   */
  private JTabbedPane views;

  public EvaluatorGui()
  {
    super("Jpea");

    this.initialize();
  }

  /**
   * Returns the quit button.
   */
  public JButton getButtonQuit()
  {
    return this.buttonQuit;
  }

  /**
   * Returns the control panel.
   */
  public JPanel getControlPanel()
  {
    return this.panelControl;
  }

  /**
   * Returns the status label.
   */
  public JLabel getStatusLabel()
  {
    return this.status;
  }

  /**
   * Returns the view pane.
   */
  public JTabbedPane getViews()
  {
    return this.views;
  }

  /**
   * Initializes this GUI.
   */
  private void initialize()
  {
    this.views = new JTabbedPane();

    // Quit button
    this.buttonQuit = new JButton("Quit");
    this.buttonQuit.addActionListener(this);

    // Status label
    this.status = new JLabel("Status: –");

    // Bottom panel
    this.panelControl = new JPanel();
    this.panelControl.setBorder(BorderFactory.createTitledBorder("controls"));
    this.panelControl.add(this.buttonQuit);

    this.panelStatus = new JPanel();
    this.panelStatus.setBorder(BorderFactory.createTitledBorder("status"));
    this.panelStatus.add(this.status);

    final JPanel panelBottom = new JPanel();
    panelBottom.setLayout(new BoxLayout(panelBottom, BoxLayout.PAGE_AXIS));
    panelBottom.add(this.panelStatus);
    panelBottom.add(this.panelControl);

    // Add panels
    this.getContentPane().setLayout(new BorderLayout());
    this.getContentPane().add(this.views, BorderLayout.CENTER);
    this.getContentPane().add(panelBottom, BorderLayout.PAGE_END);

    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  /**
   * Shows the GUI.
   */
  public void showGui()
  {
    // Disable manual tab switching
    // TODO extract into method
    for (int i = 0; i < this.views.getTabCount(); i++)
    {
      this.views.setEnabledAt(i, false);
    }

    // Resize the window
    this.pack();
    this.setVisible(true);
    this.setExtendedState(this.getExtendedState() | Frame.MAXIMIZED_BOTH);
  }
}
