package de.saviola.jpea.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a population of individuals in an evolutionary process.
 * 
 * TODO interface Population, class MapPopulation?
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation of the individuals.
 */
public class Population<G, P, V>
{
  /**
   * Population map, providing a mapping between the genotype and the
   * individuals.
   */
  private Map<G, Individual<G, P, V>> population;

  public Population()
  {
    this(new ArrayList<G>());
  }

  public Population(Collection<G> genotypes)
  {
    this.initializePopulation(genotypes);
  }

  /**
   * Initialized the population with the given genotypes.
   */
  private void initializePopulation(Collection<G> genotypes)
  {
    this.population = new HashMap<>();

    for (G genotype : genotypes)
    {
      // Create individual for the genotype and add it to our map
      this.population.put(genotype, new Individual<G, P, V>(genotype));
    }
  }

  /**
   * Returns the individual for the given genotype or null if the genotype is
   * not in the map.
   */
  public Individual<G, P, V> getIndividualByGenotype(G genotype)
  {
    return this.population.get(genotype);
  }

  /**
   * Returns a collection of all individuals of the population.
   */
  public Collection<Individual<G, P, V>> getIndividuals()
  {
    return this.population.values();
  }
}
