package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.IndividualEvaluationStatistic;

/**
 * Evaluation statistic supporting multiple evaluators.
 */
public class MultiEvaluationStatistic<G, P, V> extends
  DefaultEvaluationStatistic<G, P, V> implements
  IndividualEvaluationStatistic<G, P, V>
{

  @Override
  public void afterEvaluation(final Individual<G, P, V> individual,
    final V evaluation,
    final Object evaluatorId)
  {
    this.addRow(new Integer(System.identityHashCode(individual)),
      new Integer(this.getGeneration()),
      evaluatorId,
      evaluation);
  }

  @Override
  public void afterEvaluation(final Population<G, P, V> population)
  {
    // Nothing to do here
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "id", "generation", "evaluator", "evaluation" };
  }
}