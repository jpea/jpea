package de.saviola.jpea.core.create.function;

import de.saviola.jpea.core.create.AbstractPhenotypeCreator;

/**
 * Creates an image using an {@link ImageFunction}.
 */
public abstract class FunctionImageCreator<G extends ImageFunction<?>, P>
  extends AbstractPhenotypeCreator<G, P>
{
  /**
   * Dimension of the image to be created.
   */
  private int width, height;

  public FunctionImageCreator(int width, int height)
  {
    this.width = width;
    this.height = height;
  }

  /**
   * Returns the width of the images to be created.
   */
  public int getWidth()
  {
    return this.width;
  }

  /**
   * Returns the height of the images to be created.
   */
  public int getHeight()
  {
    return this.height;
  }
}
