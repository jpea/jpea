package de.saviola.jpea.core.evaluate;

import java.util.Comparator;

/**
 * Fitness evaluator using a comparator.
 * 
 * TODO: rename to include comparator in the name?
 */
public class JpeaFitnessEvaluator<V> implements
  FitnessEvaluator<V>
{
  private final Comparator<V> comparator;

  public JpeaFitnessEvaluator(
    final Comparator<V> comparator)
  {
    this.comparator = comparator;
  }

  @Override
  public boolean isFitter(final V o1, final V o2)
  {
    return this.comparator.compare(o1, o2) > 0;
  }
}
