package de.saviola.jpea.core.evaluate.population;

import java.util.ArrayList;
import java.util.List;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.FitnessTransformation;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;

/**
 * Weighted product population evaluator.
 * 
 * A multi-objective population evaluator using weighted products to combine
 * evaluations from individual evaluators.
 */
public class WeightedProductPopulationEvaluator<G, P> extends
  ConcurrentPopulationEvaluator<G, P, Double>
{
  /**
   * Transformations for the individual evaluations.
   * 
   * @see FitnessTransformation
   */
  private final List<FitnessTransformation> transformations = new ArrayList<>();

  @Override
  public void addIndividualEvaluator(
    final IndividualEvaluator<G, P, Double> evaluator)
  {
    this.addIndividualEvaluator(evaluator, new FitnessTransformation());
  }

  /**
   * Adds an individual evaluator with the given fitness transformation.
   */
  public void addIndividualEvaluator(
    final IndividualEvaluator<G, P, Double> evaluator,
    final FitnessTransformation transformation)
  {
    this.transformations.add(transformation);
    super.addIndividualEvaluator(evaluator);
  }

  @Override
  public synchronized void registerEvaluation(
    final Individual<G, P, Double> individual, final Double evaluation,
    final int evaluatorId)
  {
    // Check if the individual already has an evaluation
    final double evaluationOld = individual.hasEvaluation()
      ? individual.getEvaluation().doubleValue() : 1.;

    // Combine new and old evaluation using the fitness transformation
    individual.setEvaluation(new Double(evaluationOld
      * this.transformations.get(evaluatorId).transform(
        evaluation.doubleValue())));

    // Statistics will be dealt with in the superclass
    super.registerEvaluation(individual, evaluation, evaluatorId);
  }

  @Override
  public void setIndividualEvaluator(
    final IndividualEvaluator<G, P, Double> individualEvaluator)
  {
    super.setIndividualEvaluator(individualEvaluator);
    this.transformations.clear();
    this.transformations.add(new FitnessTransformation());
  }

}
