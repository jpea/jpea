package de.saviola.jpea.core.exception;

/**
 * Exception that is thrown to signal the end of the evolution.
 */
public class EndOfEvolutionException extends RuntimeException
{
  private static final long serialVersionUID = -405226002232674314L;
}
