package de.saviola.jpea.core.statistic.interfaces;

import com.brsanthu.dataexporter.DataExporter;

/**
 * Interface for data exporting statistics.
 */
public interface DataExportingStatistic
{
  /**
   * Returns the column headings for this statistic.
   */
  public String[] getColumns();

  /**
   * Returns the exporter for this statistic.
   */
  public DataExporter getExporter();

  /**
   * Sets the exporter for this statistic.
   */
  public void setExporter(DataExporter exporter);
}
