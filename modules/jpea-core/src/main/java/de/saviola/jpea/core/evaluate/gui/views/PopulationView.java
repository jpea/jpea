package de.saviola.jpea.core.evaluate.gui.views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import de.saviola.jpea.core.evaluate.gui.individual.IndividualThumbnail;

/**
 * Population view, in which information on the population are shown.
 */
public class PopulationView extends JPanel
{
  private static final long serialVersionUID = 2438385279154682769L;

  public PopulationView()
  {
    super();

    this.initialize();
  }

  /**
   * Initialize this JPanel.
   */
  private void initialize()
  {
    // Thumbnail grid
    this.setLayout(new GridBagLayout());
    this
      .setBorder(BorderFactory
        .createTitledBorder("Current image population (click images to enlarge)."));
    this.add(new JLabel("Nothing generated yet.", SwingConstants.CENTER));

  }

  /**
   * Replace the thumbnails.
   */
  public void replaceThumbnails(
    final List<? extends IndividualThumbnail<?>> individualThumbnails)
  {
    // Remove current thumbnails
    this.removeAll();

    final GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);

    // Move through thumbnails and place them in the grid bag
    int counter = 0;
    for (final IndividualThumbnail<?> thumbnail : individualThumbnails)
    {
      c.gridx = counter % 5;
      c.gridy = counter / 5;

      this.add(thumbnail, c);

      counter++;
    }

    // TODO needed?
    this.revalidate();
    this.repaint();
    //this.pack();
  }

}
