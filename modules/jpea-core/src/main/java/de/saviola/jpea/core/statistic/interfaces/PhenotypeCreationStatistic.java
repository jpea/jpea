package de.saviola.jpea.core.statistic.interfaces;

import de.saviola.jpea.core.Population;

/**
 * Interface for phenotype creation statistics.
 */
public interface PhenotypeCreationStatistic<G, P, V> extends Statistic<G, P, V>
{
  /**
   * To be called after the phenotypes have been created for the given
   * population.
   */
  public void afterPhenotypeCreation(Population<G, P, V> population);

  /**
   * To be called before the phenotype has been created for the given
   * population. 
   * @param population
   */
  public void beforePhenotypeCreation(Population<G, P, V> population);
}
