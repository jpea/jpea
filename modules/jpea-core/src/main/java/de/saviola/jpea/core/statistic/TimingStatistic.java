package de.saviola.jpea.core.statistic;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;

/**
 * General abstract timing statistic.
 * 
 * TODO do we need ThreadLocal? More general implementation?
 */
public abstract class TimingStatistic extends GenerationStatistic
{
  private final static Logger logger = LoggerFactory
    .getLogger(TimingStatistic.class);

  /**
   * Identifier of this timing statistic.
   */
  private final ThreadLocal<Object> identifier = new ThreadLocal<Object>()
  {
    @Override
    protected Object initialValue()
    {
      return new Object();
    }
  };

  /**
   * Stopwatch of this timing statistic.
   */
  private final ThreadLocal<Stopwatch> stopWatch = new ThreadLocal<Stopwatch>()
  {
    @Override
    protected Stopwatch initialValue()
    {
      return Stopwatch.createUnstarted();
    }
  };

  /**
   * Starts the timing.
   * 
   * TODO protect from multiple starts? (keep the running one valid by not
   * setting the identifier again)
   */
  public void start(final Object identifier)
  {
    // Set the identifier
    this.identifier.set(identifier);

    // Warn and stop if the stop watch was already running
    if (this.stopWatch.get().isRunning())
    {
      logger.warn("StopWatch already running! Not stopped from the last run.");
      this.stop();
    }

    // Start the stop watch
    this.stopWatch.get().start();
  }

  /**
   * Stops the timing.
   */
  public void stop()
  {
    // Stop the stop watch
    this.stopWatch.get().stop();

    // And add the row to the exporter
    this.addRow(this.identifier.get(),
      new Long(this.stopWatch.get().elapsed(TimeUnit.MILLISECONDS)));

    // Reset the stop watch to allow multiple runs
    this.stopWatch.get().reset();
  }

}
