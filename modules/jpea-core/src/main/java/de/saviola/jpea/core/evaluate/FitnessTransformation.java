package de.saviola.jpea.core.evaluate;

import de.saviola.jpea.core.evaluate.population.WeightedProductPopulationEvaluator;

/**
 * Fitness transformation used in the
 * {@link WeightedProductPopulationEvaluator}.
 * 
 * TODO rename to FitnessTransformator?
 */
public class FitnessTransformation
{
  /**
   * Exponent to be used in the fitness transformation.
   */
  private final double exponent;

  /**
   * Factor to be used in the fitness transformation.
   */
  private final double factor;

  /**
   * Offset to be used in the fitness transformation.
   */
  private final double offset;

  public FitnessTransformation()
  {
    this(1., 1., 0.);
  }

  public FitnessTransformation(final double exponent, final double factor,
    final double offset)
  {
    this.exponent = exponent;
    this.factor = factor;
    this.offset = offset;
  }

  /**
   * Transforms the given value.
   * 
   * According to the formula:
   * 
   * tranformed = (d * factor + offset)^exponent.
   */
  public double transform(final double d)
  {
    return Math.pow(d * this.factor + this.offset, this.exponent);
  }
}
