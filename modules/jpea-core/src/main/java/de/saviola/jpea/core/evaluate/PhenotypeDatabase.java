package de.saviola.jpea.core.evaluate;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 * Phenotype database used for fitness caching.
 */
public class PhenotypeDatabase<P, V>
{
  /**
   * Comparator to be used when checking whether phenotypes are equal.
   */
  private Comparator<P> comparator;

  /**
   * Whether the phenotype has hash support, i.e. its {@link #hashCode()}
   * method fulfills the contract of {@link Object#hashCode()} (see
   * especially the second point).
   * 
   * If unsure set this to false, it might break the phenotype database
   * otherwise.
   * 
   * TODO there's a bug when sometimes, wrong evaluations are associated with
   * (new) images.
   */
  private boolean hashSupport = false;

  /**
   * Contains all known phenotypes, will be used to remove the oldest once
   * the limit is reached.
   */
  private final Deque<P> knownPhenotypes = new ArrayDeque<>();

  /**
   * Maximum number of entries in the database.
   * 
   * Works by the FIFO principle once this number is reached.
   * 
   * Setting this number too high may result in bad performance since it has
   * a runtime of O(n^2) when searching the database, especially when
   * {@link #hashCode()} is set to false.
   * 
   * TODO reinsert database hits?
   */
  private final int maxNumberOfElements;

  /**
   * Maps phenotypes to evaluations.
   */
  private final Map<P, V> phenotypeEvaluations = new HashMap<>();

  public PhenotypeDatabase(final int maxNumberOfElements,
    final boolean hashSupport)
  {
    this.maxNumberOfElements = maxNumberOfElements;
    this.hashSupport = hashSupport;
  }

  /**
   * Adds a phenotype to the database.
   * 
   * If the database has reached its size limit, the oldest element is removed.
   */
  private void addPhenotype(final P phenotype)
  {
    this.knownPhenotypes.addLast(phenotype);

    // If the maximum size has been reached, remove the oldest phenotype
    if (this.knownPhenotypes.size() > this.maxNumberOfElements)
    {
      this.removePhenotype(null);
    }
  }

  /**
   * Returns the phenotype found in the database or the given phenotype if
   * none was found.
   */
  private P findKnownPhenotype(P phenotype)
  {
    // Some classes do not support hash codes, so we have to explicitly check
    // for equality
    for (final P knownPhenotype : this.knownPhenotypes)
    {
      // TODO order? would you specify a comparator if equals works?
      if (knownPhenotype == phenotype ||
        knownPhenotype.equals(phenotype) ||
        this.comparator != null
        && this.comparator.compare(phenotype, knownPhenotype) == 0)
      {
        phenotype = knownPhenotype;

        break;
      }
    }

    return phenotype;
  }

  /**
   * Returns the comparator.
   */
  public Comparator<P> getComparator()
  {
    return this.comparator;
  }

  /**
   * Returns the evaluation for the given phenotype or defaultEvaluation, if
   * the phenotype was not among the known phenotypes.
   */
  public V getEvaluation(P phenotype, final V defaultEvaluation)
  {
    // If there's no hash support, we have to search through the database the
    // hard way
    if (!this.hashSupport)
    {
      phenotype = this.findKnownPhenotype(phenotype);
    }

    // Try to fetch an existing evaluation from the database
    final V evaluation = this.phenotypeEvaluations.get(phenotype);

    // Return the fetched evaluation or the default evaluation if the phenotype
    // is not in the database yet
    return evaluation != null ? evaluation : defaultEvaluation;
  }

  /**
   * Stores the evaluation for the phenotype, overwriting the existing value.
   */
  public void putEvaluation(P phenotype, final V evaluation)
  {
    // Again, no hash support means searching through the database
    if (!this.hashSupport)
    {
      phenotype = this.findKnownPhenotype(phenotype);
    }

    // Fetch the old value from the database
    final V oldValue = this.phenotypeEvaluations.put(phenotype, evaluation);

    // If there was a previous mapping, remove it before adding it again
    if (oldValue != null)
    {
      this.removePhenotype(phenotype);
    }

    this.addPhenotype(phenotype);
  }

  /**
   * Removes the given phenotype or the oldest, if null was given.
   */
  private void removePhenotype(P phenotype)
  {
    // TODO extract into remove[First]Phenotype() method without parameters
    if (phenotype == null)
    {
      phenotype = this.knownPhenotypes.getFirst();
    }

    this.knownPhenotypes.remove(phenotype);

    this.phenotypeEvaluations.remove(phenotype);
  }

  /**
   * Sets the comparator.
   */
  public void setComparator(final Comparator<P> comparator)
  {
    this.comparator = comparator;
  }
}