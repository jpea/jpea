package de.saviola.jpea.core.evaluate.population;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;
import de.saviola.jpea.core.statistic.StatisticsCollector;

/**
 * Interface for population evaluators.
 * 
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation type of the individuals.
 */
public interface PopulationEvaluator<G, P, V>
{
  /**
   * Evaluates all individuals of the given population at once.
   * 
   * This is useful for multi-threaded evaluation or when the individuals'
   * evaluations are interdependent.
   * 
   * The default implementation is to call
   * {@link #evaluateIndividual(Individual)} for each individual of the
   * population and store the evaluation in the individuals.
   */
  public void evaluatePopulation(Population<G, P, V> population);

  /**
   * Returns the individual evaluator of this population evaluator.
   */
  public IndividualEvaluator<G, P, V> getIndividualEvaluator();

  /**
   * Returns the statistics collector or null of none was registered yet.
   */
  public StatisticsCollector<G, P, V> getStatisticsCollector();

  /**
   * Set the individual evaluator of this population evaluator.
   */
  public void setIndividualEvaluator(
    IndividualEvaluator<G, P, V> individualEvaluator);

  /**
   * Sets the statistics collector.
   */
  public void setStatisticsCollector(StatisticsCollector<G, P, V> statistics);
}
