package de.saviola.jpea.core.evaluate.gui.individual;

/**
 * An individual viewer consisting of several individual viewers.
 */
public interface CompositeIndividualViewer<G, P, V> extends
  IndividualViewer<G, P, V>
{
  /**
   * Adds an individual viewer.
   */
  public void addIndividualViewer(IndividualViewer<G, P, V> viewer);
}
