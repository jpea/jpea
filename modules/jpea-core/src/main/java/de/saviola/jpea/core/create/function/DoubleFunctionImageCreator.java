package de.saviola.jpea.core.create.function;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;

import de.saviola.jpea.core.Individual;

/**
 * Image creator that uses an image function returning double values.
 */
public class DoubleFunctionImageCreator extends
  FunctionImageCreator<ImageFunction<Double>, BufferedImage>
{

  public DoubleFunctionImageCreator(final int width, final int height)
  {
    super(width, height);
  }

  /**
   * Creates the phenotype for the given individual.
   */
  @Override
  public void createForIndividual(
    final Individual<ImageFunction<Double>, BufferedImage, ?> individual)
  {
    // TODO return the image and remove this check?
    if (individual.hasPhenotype())
    {
      return;
    }

    BufferedImage image = null;

    try
    {
      // The color array of the new image
      final int[] colorArray = new int[this.getWidth() * this.getHeight()];

      // The image function used to create the image
      final ImageFunction<Double> function = individual.getGenotype();

      // Loop variables
      double v;
      final int width = this.getWidth(), height = this.getHeight();

      double xScaled = 0., yScaled = 0.;

      // For each pixel of the image, the image function is evaluated
      for (int x = 0; x < width; x++)
      {
        for (int y = 0; y < height; y++)
        {
          xScaled = (double) x / width * 2 - 1;
          yScaled = (double) y / height * 2 - 1;

          // Scale from -1..1 to 0..1
          v = (function.evaluateAt(xScaled, yScaled).doubleValue() + 1) / 2;

          // Color is generated from a single double value
          colorArray[y * width + x] =
            0xFF000000 + (int) (0xFFFFFF * v);
        }
      }

      // Hopefully one of the fastest ways of creating a buffered image from
      // an array
      final int[] bitMasks = new int[] { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
      final SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(
        DataBuffer.TYPE_INT, width, height, bitMasks);
      final DataBufferInt db = new DataBufferInt(colorArray, colorArray.length);
      final WritableRaster wr =
        Raster.createWritableRaster(sm, db, new Point());
      image = new BufferedImage(ColorModel.getRGBdefault(), wr, true, null);
    }
    // If anything has happened it was probably in evaluateAt() of the image
    // function, meaning the genotype is defect, no phenotype will be set
    catch (final RuntimeException e)
    {
      // TODO log? Something other than null?
      image = null;
    }

    // Set the generated image (or null) as the phenotype
    individual.setPhenotype(image);
  }

}
