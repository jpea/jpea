package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.NormalDistribution;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Ralph image evaluator, using color gradients and distributions to evaluate
 * an image.
 */
public class RalphEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  public RalphEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Builds the histogramm for the given array and with given bin width.
   */
  private Map<Double, Double> buildHistogram(final double response[],
    final double binWidth)
  {
    final Map<Double, Double> histogram = new HashMap<>();
    Double bin, oldValue;

    for (int i = 0; i < response.length; i++)
    {
      bin = new Double(Math.rint(response[i] / binWidth));
      oldValue = histogram.get(bin);

      histogram.put(bin, new Double(oldValue != null ? oldValue.doubleValue()
        + response[i] : response[i]));
    }

    return histogram;
  }

  /**
   * Calculates the deviation for the given response array and mean.
   */
  private double calculateDeviation(final double response[], final double mean)
  {
    double sum = 0., sum2 = 0.;

    for (int i = 0; i < response.length; i++)
    {
      // TODO extract, is calculated twice (see above)?
      sum += response[i];
      sum2 += response[i] * (response[i] - mean) * (response[i] - mean);
    }

    return Math.sqrt(sum2 / sum);
  }

  /**
   * Calculates deviation from normality (relative entropy).
   */
  private double calculateDeviationFromNormality(final double mean,
    final double deviation,
    final double binWidth, final Map<Double, Double> actualDistribution,
    final int width,
    final int height)
  {
    final NormalDistribution normalDistribution =
      new NormalDistribution(mean, deviation);

    final double binWidthHalf = binWidth / 2;
    final long numPixel = width * height;

    double sum = 0., q, p, bin;
    for (final Map.Entry<Double, Double> entry : actualDistribution.entrySet())
    {
      bin = entry.getKey().doubleValue();
      p = entry.getValue().doubleValue() / numPixel;

      q = normalDistribution.probability(bin * binWidth - binWidthHalf,
        bin * binWidth + binWidthHalf);

      if (q != 0 && p != 0)
      {
        sum += p * Math.log10(p / q);
      }
    }

    return 1000 * sum;
  }

  /**
   * Calculates the mean of the normal distribution for the given array.
   */
  private double calculateMean(final double response[])
  {
    double sum = 0., sum2 = 0.;

    for (int i = 0; i < response.length; i++)
    {
      sum += response[i];
      sum2 += response[i] * response[i];
    }

    return sum2 / sum;
  }

  /**
   * Calculates a single pixel's response value.
   * 
   * TODO why pass whole array along every time?
   */
  private double calculatePixelResponse(final int x, final int y,
    final int[] colorArray, final int w,
    final int h)
  {
    final int[] shifts = new int[] { 16, 8, 0 };
    final int[] offsets = new int[] { 0x00FF0000, 0x0000FF00, 0x000000FF };

    double stimulus = 0.;
    // TODO correct?
    final double d2 = Math.pow(Math.sqrt(w * w + h * h) / 2000, 2);

    for (int i = 0; i < 3; i++)
    {
      stimulus +=
        (Math.pow(((colorArray[x * w + y] & offsets[i]) >>> shifts[i]) -
          ((colorArray[(x + 1) * w + y + 1] & offsets[i]) >>> shifts[i]), 2)
          +
          Math.pow(((colorArray[x * w + y + 1] & offsets[i]) >>> shifts[i]) -
            ((colorArray[(x + 1) * w + y] & offsets[i]) >>> shifts[i]), 2))
          / d2;
    }

    stimulus = Math.sqrt(stimulus);

    final double response =
      stimulus == 0 ? 0 : Math.log10(stimulus / 2);

    return response;
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // No phenotype means worst evaluation
    if (!individual.hasPhenotype())
    {
      return this.getWorstEvaluation();
    }

    // Extract color array
    final BufferedImage image = individual.getPhenotype();
    final int[] colorArray =
      ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    final int width = image.getWidth(), height = image.getHeight();
    final double[] response = new double[width * height];

    // Calculate pixel responses
    for (int x = 0; x < height - 1; x++)
    {
      for (int y = 0; y < width - 1; y++)
      {
        response[x * width + y] =
          this.calculatePixelResponse(x, y, colorArray, width, height);
      }
    }

    // Calculate parameters of the normal distribution
    final double mean = this.calculateMean(response), deviation =
      this.calculateDeviation(response, mean);

    final double binWidth = deviation / 100;

    // Build histogram
    final Map<Double, Double> histogram =
      this.buildHistogram(response, binWidth);

    final Double evaluation = new Double(this.calculateDeviationFromNormality(
      mean, deviation, binWidth, histogram, width, height));

    return evaluation.doubleValue() > 0 ? evaluation
      : this.getWorstEvaluation();
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(0.);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }
}