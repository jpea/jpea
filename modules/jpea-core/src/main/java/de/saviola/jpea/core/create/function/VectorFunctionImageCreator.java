package de.saviola.jpea.core.create.function;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.util.Vector;

import de.saviola.jpea.core.Individual;

/**
 * Phenotype creator that uses an {@link ImageFunction} which returns vectors
 * to create a {@link BufferedImage} instance.
 */
public class VectorFunctionImageCreator extends
  FunctionImageCreator<ImageFunction<Vector<Number>>, BufferedImage>
{
  /**
   * @see FunctionImageCreator#FunctionImageCreator(int, int)
   */
  public VectorFunctionImageCreator(final int width, final int height)
  {
    super(width, height);
  }

  /**
   * Creates the phenotype for the given individual.
   */
  @Override
  public void createForIndividual(
    final Individual<ImageFunction<Vector<Number>>, BufferedImage, ?> individual)
  {
    BufferedImage image = null;

    try
    {
      // The color array of the new image
      final int[] colorArray = new int[this.getWidth() * this.getHeight()];

      // The image function used to create the image
      final ImageFunction<Vector<Number>> function = individual.getGenotype();

      // Loop variables
      Vector<? extends Number> v;
      final int width = this.getWidth(), height = this.getHeight();
      int r = 0, g = 0, b = 0;

      double xScaled = 0., yScaled = 0.;

      // For each pixel of the image, the image function is evaluated
      for (int x = 0; x < width; x++)
      {
        for (int y = 0; y < height; y++)
        {
          xScaled = (double) x / width * 2 - 1;
          yScaled = (double) y / height * 2 - 1;

          v = function.evaluateAt(xScaled, yScaled);

          r = (int) Math.abs(v.elementAt(0).doubleValue() * 255) & 0xFF;
          g = (int) Math.abs(v.elementAt(1).doubleValue() * 255) & 0xFF;
          b = (int) Math.abs(v.elementAt(2).doubleValue() * 255) & 0xFF;

          // Out of the three components of the vector, an int RGB
          // representation is generated
          colorArray[y * width + x] =
            0xFF000000
              + (r << 16)
              + (g << 8)
              + b;
        }
      }

      // Fast creation of BufferedImage from array
      final int[] bitMasks = new int[] { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
      final SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(
        DataBuffer.TYPE_INT, width, height, bitMasks);
      final DataBufferInt db = new DataBufferInt(colorArray, colorArray.length);
      final WritableRaster wr =
        Raster.createWritableRaster(sm, db, new Point());
      image = new BufferedImage(ColorModel.getRGBdefault(), wr, true, null);
    }
    // If anything has happened it was probably in evaluateAt() of the image
    // function, meaning the genotype is defect, no phenotype will be set
    catch (final RuntimeException e)
    {
      // TODO log? Something other than null?
      image = null;
    }

    // Set the generated image (or null) as the phenotype
    individual.setPhenotype(image);
  }
}
