package de.saviola.jpea.core.evaluate.gui.individual;

import de.saviola.jpea.core.Individual;

/**
 * Interface to be implemented by all GUI components which can show detail
 * information on an individual.
 * 
 * @param <G> Genotype of the individual
 * @param <P> Phenotype of the individual
 * @param <V> Evaluation type of the individual
 */
public interface IndividualViewer<G, P, V>
{
  /**
   * Shows information on the given individual.
   */
  public void showIndividual(Individual<G, P, V> individual);
}
