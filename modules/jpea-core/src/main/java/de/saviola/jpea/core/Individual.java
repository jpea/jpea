package de.saviola.jpea.core;

/**
 * Represents an individual in an evolutionary process, conatining its
 * genotype, phenotype and evaluation.
 * 
 * @param <G> Genotype of the individual.
 * @param <P> Phenotype of the individual.
 * @param <V> Evaluation of the individual.
 */
public class Individual<G, P, V> implements Cloneable
{
  /**
   * Evaluation of the individual.
   * 
   * @see #getEvaluation()
   * @see #hasEvaluation()
   * @see #setEvaluation(V)
   */
  private V evaluation;

  /**
   * Genotype of the individual.
   * 
   * Must never be null, will be set in the constructor.
   * 
   * @see #getGenotype()
   * @see #Individual(G)
   */
  private final G genotype;

  /**
   * Phenotype of the individual.
   * 
   * @see #getPhenotype()
   * @see #hasPhenotype()
   * @see #setPhenotype(V)
   */
  private P phenotype;

  /**
   * Creates an individual with the given genotype.
   * 
   * @param genotype Must not be null.
   */
  public Individual(final G genotype)
  {
    // TODO parameter exception
    if (genotype == null)
    {
      throw new RuntimeException("Genotype of an individual must not be null");
    }

    this.genotype = genotype;
  }

  /**
   * Clones an individual.
   */
  @Override
  public Individual<G, P, V> clone()
  {
    final Individual<G, P, V> clone = new Individual<>(this.genotype);
    clone.setPhenotype(this.getPhenotype());
    clone.setEvaluation(this.getEvaluation());

    return clone;
  }

  /**
   * Returns the evalutation of the individual or null if it was not yet
   * evaluated.
   * 
   * @see #hasEvaluation()
   */
  public V getEvaluation()
  {
    return this.evaluation;
  }

  /**
   * Returns the genotype of the individual.
   */
  public G getGenotype()
  {
    return this.genotype;
  }

  /**
   * Returns the phenotype of the individual or null if the phenotype has not
   * yet been created.
   * 
   * @see #hasPhenotype()
   */
  public P getPhenotype()
  {
    return this.phenotype;
  }

  /**
   * Returns whether the individual has already been evaluated.
   */
  public boolean hasEvaluation()
  {
    return this.evaluation != null;
  }

  /**
   * Returns whether the phenotype for the individual has already been created.
   */
  public boolean hasPhenotype()
  {
    return this.phenotype != null;
  }

  /**
   * Sets the evaluation of the individual.
   */
  public void setEvaluation(final V value)
  {
    this.evaluation = value;
  }

  /**
   * Sets the phenotype of the individual.
   */
  public void setPhenotype(final P phenotype)
  {
    this.phenotype = phenotype;
  }
}
