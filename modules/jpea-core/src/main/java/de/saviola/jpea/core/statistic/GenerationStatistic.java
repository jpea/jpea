package de.saviola.jpea.core.statistic;

/**
 * TODO documentation
 */
public abstract class GenerationStatistic extends
  AbstractDataExportingStatistic
{
  private int generation = 0;

  public int getGeneration()
  {
    return this.generation;
  }

  public void incrementGeneration()
  {
    this.generation++;
  }
}
