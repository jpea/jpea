package de.saviola.jpea.core.application;

/**
 * Interface for the driver of a GP framework.
 * 
 * It is supposed to encapsulte the whole evolutionary process and only calling
 * the methods
 * {@link EvolutionEngine#processPopulation(de.saviola.jpea.core.Population)}
 * and
 * {@link EvolutionEngine#processIndividual(de.saviola.jpea.core.Individual)}
 * on the given {@link EvolutionEngine} when necessary.
 * 
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation type of the individuals.
 */
public interface FrameworkDriver<G, P, V>
{
  /**
   * Initialization hook, e.g. for configuration of the framework.
   */
  public void initialize() throws Exception;

  /**
   * Start the evolutionary process.
   */
  public void startEvolution(EvolutionEngine<G, P, V> engine) throws Exception;
}
