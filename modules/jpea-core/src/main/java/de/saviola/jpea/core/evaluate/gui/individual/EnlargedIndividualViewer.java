package de.saviola.jpea.core.evaluate.gui.individual;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import de.saviola.jpea.core.Individual;

/**
 * Individual viewer which will show an individual at a size of 500x500.
 * 
 * TODO enforce size
 */
public class EnlargedIndividualViewer<G, V> implements
  DisplayableIndividualViewer<G, BufferedImage, V>
{
  /**
   * Image panel which holds the individual's phenotype.
   */
  private final MarvinImagePanel imagePanel;

  public EnlargedIndividualViewer()
  {
    this.imagePanel = new MarvinImagePanel();
    this.imagePanel.setPreferredSize(new Dimension(550, 550));
  }

  /**
   * Returns the component of the individual viewer (i.e. the MarvinImagePanel)
   */
  @Override
  public Component getComponent()
  {
    return this.imagePanel;
  }

  /**
   * Shows the given individual.
   */
  @Override
  public void showIndividual(final Individual<G, BufferedImage, V> individual)
  {
    this.imagePanel.setImage(new MarvinImage(individual.getPhenotype()));
    this.imagePanel.update();
  }
}
