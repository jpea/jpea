package de.saviola.jpea.core.create.function;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;

import org.apache.commons.math3.complex.Complex;

import de.saviola.jpea.core.Individual;

/**
 * Function image creator for fractals.
 * 
 * Currently unused.
 */
public class FractalFunctionImageCreator extends
  FunctionImageCreator<FractalImageFunction<Complex>, BufferedImage>
{
  public FractalFunctionImageCreator(final int width, final int height)
  {
    super(width, height);
  }

  /**
   * Creates phenotype for the given individual.
   */
  @Override
  public void createForIndividual(
    final Individual<FractalImageFunction<Complex>, BufferedImage, ?> individual)
  {
    // TODO return the image and remove this check?
    if (individual.hasPhenotype())
    {
      return;
    }

    final int width = this.getWidth(), height = this.getHeight();

    final FractalImageFunction<Complex> function = individual.getGenotype();

    // Extract parameters from the FractalImageFunction
    final int backgroundColor = 0xFF000000 +
      (int) (0xFFFFFF * Math.min(1, Math.abs(function.getBackgroundValue())));
    final Complex z = function.getStartingPoint();

    // Initialize loop variables
    final double xStart = Double.isNaN(z.getReal()) ? 0. : z.getReal(), yStart =
      Double.isNaN(z.getImaginary()) ? 0. : z.getImaginary();
    final double stepSize = individual.getGenotype().getStepSize() > 0 ?
      individual.getGenotype().getStepSize() : 1.;
    final int iterations = 15;

    Complex zNew = z;

    final int[] colorArray = new int[this.getWidth() * this.getHeight()];
    double distance;
    final double distanceThreshold = Math.abs(z.abs()
      - function.evaluateAt(z.getReal(), z.getImaginary()).abs()) * 3;
    double x, y, xTmp, yTmp;

    BufferedImage image;

    try
    {
      for (int i = 0; i < width; i++)
      {
        x = xStart + stepSize * i;

        for (int j = 0; j < height; j++)
        {
          y = yStart + stepSize * j;

          colorArray[j * width + i] = backgroundColor;

          xTmp = x;
          yTmp = y;

          for (int k = 0; k < iterations; k++)
          {
            zNew = function.evaluateAt(xTmp, yTmp);

            distance = Math.abs(z.abs() - zNew.abs());

            if (distance > distanceThreshold)
            {
              colorArray[j * width + i] = 0xFF000000 + (int) (0xFFFFFF *
                (k / (double) iterations));
              break;
            }

            xTmp = zNew.getReal();
            yTmp = zNew.getImaginary();
          }
        }
      }

      final int[] bitMasks = new int[] { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
      final SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(
        DataBuffer.TYPE_INT, width, height, bitMasks);
      final DataBufferInt db = new DataBufferInt(colorArray, colorArray.length);
      final WritableRaster wr =
        Raster.createWritableRaster(sm, db, new Point());
      image = new BufferedImage(ColorModel.getRGBdefault(), wr, true, null);
      individual.setPhenotype(image);
    }
    catch (final Exception e)
    {
      // TODO handle
      e.printStackTrace();
    }
  }
}
