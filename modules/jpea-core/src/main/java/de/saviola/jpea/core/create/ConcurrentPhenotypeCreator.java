package de.saviola.jpea.core.create;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;

/**
 * Abstract phenotype creator which provides a concurrent implementation of
 * {@link PhenotypeCreator#createForPopulation(Population)}.
 */
public abstract class ConcurrentPhenotypeCreator<G, P> extends
  AbstractPhenotypeCreator<G, P>
{
  /**
   * Pool size of the thread pool.
   */
  private final int poolSize;

  public ConcurrentPhenotypeCreator()
  {
    // TODO DRY
    this.poolSize = Runtime.getRuntime().availableProcessors() * 2 + 1;
  }

  /**
   * Create phenotypes for all individuals of the population.
   */
  @Override
  public void createForPopulation(final Population<G, P, ?> population)
  {
    final ExecutorService threadPool =
      Executors.newFixedThreadPool(this.poolSize);

    // Walk through all individuals
    for (final Individual<G, P, ?> individual : population.getIndividuals())
    {
      // And submit a new task to the thread pool
      threadPool.submit(new Callable<Individual<G, P, ?>>()
      {
        /**
         * Create phenotype for individual.
         */
        @Override
        public Individual<G, P, ?> call() throws Exception
        {
          ConcurrentPhenotypeCreator.this.createForIndividual(individual);

          return individual;
        }
      });
    }

    // Shutdown the thread pool and await its termination
    threadPool.shutdown();

    while (!threadPool.isTerminated())
    {
      try
      {
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
      }
      catch (final InterruptedException e)
      {
        // TODO Handle exception
        e.printStackTrace();
      }
    }
  }
}
