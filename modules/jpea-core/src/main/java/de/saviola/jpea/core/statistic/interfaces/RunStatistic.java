package de.saviola.jpea.core.statistic.interfaces;

/**
 * Interface for evolutionary run statistics.
 */
public interface RunStatistic<G, P, V> extends Statistic<G, P, V>
{
  /**
   * To be called after the run has ended.
   */
  public void afterRunEnd();

  /**
   * To be called before the run starts.
   */
  public void beforeRunStart();
}
