package de.saviola.jpea.core.statistic;

import com.brsanthu.dataexporter.DataExporter;

import de.saviola.jpea.core.statistic.interfaces.DataExportingStatistic;

/**
 * Abstract superclass for data exporting statistics.
 */
abstract public class AbstractDataExportingStatistic implements
  DataExportingStatistic
{
  /**
   * Exporter to be used for the statistics.
   * 
   * @see DataExporter
   */
  private DataExporter exporter;

  /**
   * Used to pass rows to the exporter, synchronized to allow usage in
   * concurrent scenarios.
   */
  protected synchronized void addRow(final Object... row)
  {
    this.exporter.addRow(row);
  }

  @Override
  public DataExporter getExporter()
  {
    return this.exporter;
  }

  @Override
  public void setExporter(final DataExporter exporter)
  {
    this.exporter = exporter;

    // Try to add the columns
    try
    {
      this.exporter.addColumn(this.getColumns());
    }
    catch (final IllegalStateException e)
    {
      // Do nothing if the exporter has already been exporting
    }
  }
}
