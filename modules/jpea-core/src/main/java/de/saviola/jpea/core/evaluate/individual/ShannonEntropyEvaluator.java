package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.jhlabs.image.GrayscaleFilter;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Shannon entropy evaluator.
 * 
 * Calculates the entropy of an image.
 */
public class ShannonEntropyEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  public ShannonEntropyEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Calculates the shannon entropy of the given image.
   */
  private double calculateShannonEntropy(final BufferedImage image)
  {
    // Transform to grayscale
    final GrayscaleFilter filter = new GrayscaleFilter();
    final BufferedImage imageGray = filter.filter(image, null);

    final int[] colorArray =
      ((DataBufferInt) imageGray.getRaster().getDataBuffer()).getData();

    final int numPixels = colorArray.length;

    final int[] histogram = new int[256];

    // Build histogram
    for (int i = 0; i < numPixels; i++)
    {
      histogram[colorArray[i] & 0x000000FF]++;
    }

    double entropy = 0.;
    double pi;

    // Calculate entropy
    for (int i = 0; i < 256; i++)
    {
      pi = histogram[i] / (double) numPixels;
      entropy += pi > 0 ? pi * Math.log(pi) : 0.;
    }

    return entropy < 0 ? -1 * entropy : 0.;
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    return new Double(this.calculateShannonEntropy(individual.getPhenotype()));
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(0.);
  }

}
