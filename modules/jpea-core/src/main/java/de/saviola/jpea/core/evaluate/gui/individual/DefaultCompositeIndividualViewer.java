package de.saviola.jpea.core.evaluate.gui.individual;

import java.util.ArrayList;
import java.util.List;

import de.saviola.jpea.core.Individual;

/**
 * Simple composite implementation for multiple individual viewers.
 */
public class DefaultCompositeIndividualViewer<G, P, V> implements
  CompositeIndividualViewer<G, P, V>
{
  /**
   * List of individual viewers.
   */
  private final List<IndividualViewer<G, P, V>> viewers = new ArrayList<>();

  /**
   * Adds a new individual viewer.
   */
  @Override
  public void addIndividualViewer(final IndividualViewer<G, P, V> viewer)
  {
    // Will not add itself
    if (!viewer.equals(this))
    {
      this.viewers.add(viewer);
    }
  }

  /**
   * Shows the given individual in all viewers.
   */
  @Override
  public void showIndividual(final Individual<G, P, V> individual)
  {
    for (final IndividualViewer<G, P, V> viewer : this.viewers)
    {
      viewer.showIndividual(individual);
    }
  }
}
