package de.saviola.jpea.core.evaluate.individual.compare;

import java.util.Comparator;

import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;

/**
 * Comparator for double values with dnyamic behavior depending on the given
 * optimization goal.
 */
public class DynamicDoubleComparator<G, P> implements Comparator<Double>
{
  /**
   * The optimization goal.
   */
  private double goal = 0.;

  /**
   * The multiplier, used for infinitve optimization goals.
   */
  private double multiplier = 0.;

  public DynamicDoubleComparator(final double optimizationGoal)
  {
    // If the optimization goal is infinite, use the multiplier
    if (Double.isInfinite(optimizationGoal))
    {
      this.multiplier = Math.copySign(1., optimizationGoal) * -1;
    }
    // Otherwise use the optimization goal
    else
    {
      this.goal = optimizationGoal;
    }
  }

  /**
   * Constructor for evaluators, extracting the optimization goal.
   */
  public DynamicDoubleComparator(
    final IndividualEvaluator<G, P, Double> evaluator)
  {
    this(evaluator.getBestEvaluation().doubleValue());
  }

  /**
   * Compares the two given values according to the optimization goal.
   * 
   * Some examples:
   * 
   * optimization goal: 10, multiplier = 0, d1 = 5, d2 = -1
   * transformed values: d1 = |5-10| = 5, d2 = |-1-10| = 11
   * comparison result: -1*-1 = 1 -> d1 is greater, because it is better, i.e.
   * closer to the goal.
   * 
   * optimization goal = {@link Double#NEGATIVE_INFINITY}, goal = 0,
   * multiplier = -1*-1 = 1 (see above), d1 = 5, d2 = -1
   * Transformed values: d1 = 5*1 = 5, d2 = -1*1 = 1
   * comparison result: 1*-1 = -1 -> d2 is greater, because it is closer to
   * {@link Double#NEGATIVE_INFINITY}.
   */
  @Override
  public int compare(final Double d1, final Double d2)
  {
    // If one of the numbers is not a number, the one that is a number is
    // greater
    if (d1.isNaN() || d2.isNaN())
    {
      return d1.isNaN() ? -1 : 1;
    }

    // Transform the numbers
    final Double dt1 = this.transform(d1.doubleValue()), dt2 =
      this.transform(d2.doubleValue());

    // Return the negated result
    return dt1.compareTo(dt2) * -1;
  }

  private Double transform(final double d)
  {
    return new Double(this.multiplier != 0 ? d * this.multiplier : Math.abs(d
      - this.goal));
  }
}
