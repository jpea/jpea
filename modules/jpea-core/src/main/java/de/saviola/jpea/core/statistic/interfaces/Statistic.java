package de.saviola.jpea.core.statistic.interfaces;

/**
 * Tag interface for statistics.
 * 
 * @param <G> Genotypes of the involved individuals.
 * @param <P> Phenotypes of the involved individuals.
 * @param <V> Evaluation types of the involved individuals.
 */
public interface Statistic<G, P, V>
{

}
