package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import com.jhlabs.image.GrayscaleFilter;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;
import fic.lib.Compressor;
import fic.lib.Decompressor;
import fic.lib.comparators.ImageComparator;
import fic.lib.comparators.Metric;
import fic.lib.tilers.RectangularPixelTiler;
import fic.lib.transformations.AffineRotateQuadrantsTransform;
import fic.lib.transformations.FlipTransform;
import fic.lib.transformations.FlopTransform;
import fic.lib.transformations.ImageTransform;
import fic.lib.transformations.NoneTransform;
import fic.lib.transformations.ScaleTransform;

/**
 * Calculates processing complexity of an image using fractal compression.
 * 
 * Currently not used (fractal compression is too slow).
 */
public class ProcessingComplexityEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  private Compressor compressor;
  private Decompressor decompressor;

  public ProcessingComplexityEvaluator()
  {
    this.initialize();
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  private Double calculateProcessingComplexity(
    final Individual<G, BufferedImage, Double> individual)
  {
    final GrayscaleFilter filter = new GrayscaleFilter();
    final BufferedImage grayscaledImage =
      filter.filter(individual.getPhenotype(), null);

    final BufferedImage compressionResult =
      this.decompressor.decompress(this.compressor.compress(grayscaledImage));

    final RmseEvaluator<G> rmseEvaluator = new RmseEvaluator<>(grayscaledImage);

    final Individual<G, BufferedImage, Double> individualCompressed =
      individual.clone();
    individualCompressed.setPhenotype(compressionResult);

    return rmseEvaluator.evaluateIndividual(individualCompressed);
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    return this.calculateProcessingComplexity(individual);
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(0.);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }

  private void initialize()
  {
    final Set<ImageTransform> imageTransformations = new HashSet<>();
    imageTransformations.add(new NoneTransform());
    imageTransformations.add(new FlipTransform());
    imageTransformations.add(new FlopTransform());
    imageTransformations.add(new AffineRotateQuadrantsTransform(1));
    imageTransformations.add(new AffineRotateQuadrantsTransform(2));
    imageTransformations.add(new AffineRotateQuadrantsTransform(3));

    final Set<BufferedImageOp> imageOperations = new HashSet<>();
    imageOperations.add(new GrayscaleFilter());

    this.compressor = new Compressor(
      new ScaleTransform(.2, .2),
      new RectangularPixelTiler(10, 10),
      new ImageComparator(Metric.AE, 20),
      imageTransformations,
      imageOperations,
      null);

    this.decompressor = new Decompressor(new Observer()
    {
      @Override
      public void update(final Observable arg0, final Object arg1)
      {
        // Nothing to do here
        // TODO report bug back to fic
      }
    });
  }

}
