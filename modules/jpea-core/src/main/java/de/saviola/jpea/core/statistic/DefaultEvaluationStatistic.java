package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.PopulationEvaluationStatistic;

/**
 * The default evaluation statistic.
 */
public class DefaultEvaluationStatistic<G, P, V> extends
  GenerationStatistic implements PopulationEvaluationStatistic<G, P, V>
{
  @Override
  public void afterEvaluation(final Population<G, P, V> population)
  {
    // Add rows for the evaluations of all individuals in the given population
    for (final Individual<G, P, V> individual : population.getIndividuals())
    {
      this.addRow(new Integer(System.identityHashCode(individual)),
        new Integer(this.getGeneration()),
        individual.getEvaluation());
    }
  }

  @Override
  public void beforeEvaluation(final Population<G, P, V> population)
  {
    this.incrementGeneration();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "id", "generation", "evaluation" };
  }
}
