package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.jhlabs.image.GrayscaleFilter;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Global contrast factor (GCF) evaluator.
 * 
 * TODO: Reference literature
 */
public class GlobalContrastFactorEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  /**
   * The target GCF.
   */
  private final Double optimizationGoal;

  public GlobalContrastFactorEvaluator(final double optimizationGoal)
  {
    this.optimizationGoal = new Double(optimizationGoal);
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Builds a superpixel array with given pixel size from the given luminance
   * array.
   */
  private double[] buildSuperpixelArray(final double[] luminanceArray,
    final int pixelSize,
    final int w, final int h)
  {
    // Pixel size of 1 means no changes to the luminance array
    if (pixelSize == 1)
    {
      return luminanceArray;
    }

    // Calculate the new image size
    final int newWidth = (int) Math.ceil((double) w / pixelSize);
    final int newHeight = (int) Math.ceil((double) h / pixelSize);

    final double[] superpixelArray = new double[newWidth * newHeight];

    // TODO one loop?
    // Create all superpixels
    for (int i = 0; i < h; i += pixelSize)
    {
      for (int j = 0; j < w; j += pixelSize)
      {
        superpixelArray[i / pixelSize * newHeight + j / pixelSize] =
          this.createSuperPixel(luminanceArray, pixelSize, j, i, h);
      }
    }

    return superpixelArray;
  }

  /**
   * Calculates the average local contrast for the given luminance array.
   */
  private double calculateAverageLocalContrast(final double[] luminanceArray,
    final int w,
    final int h)
  {
    final double[] perceptualLuminanceArray = new double[luminanceArray.length];

    // Transform luminance to perceptual luminance
    for (int i = 0; i < luminanceArray.length; i++)
    {
      perceptualLuminanceArray[i] = 100 * Math.sqrt(luminanceArray[i]);
    }

    final double[] pLA = perceptualLuminanceArray;

    // Calculate the local contrast sum
    double localContrastSum = 0.;

    // Regular pixels (no special cases)
    for (int i = 1; i < h - 1; i++)
    {
      for (int j = 1; j < w - 1; j++)
      {
        localContrastSum +=
          (Math.abs(pLA[i * h + j] - pLA[i * h + j - 1]) +
            Math.abs(pLA[i * h + j] - pLA[i * h + j + 1]) +
            Math.abs(pLA[i * h + j] - pLA[i * h + j - w]) +
          Math.abs(pLA[i * h + j] - pLA[i * h + j + w])) / 4;
      }
    }

    double localContrast;
    // Linear index
    int li;
    int summands;

    // Now for all the pixels at the edges
    for (int i = 0; i < h; i += h - 1)
    {
      for (int j = 0; j < w; j += w - 1)
      {
        summands = 4;

        li = i * h + j;
        localContrast = Math.abs(pLA[li] - pLA[j == 0 ? li : li - 1]);
        localContrast += Math.abs(pLA[li] - pLA[j == w - 1 ? li : li + 1]);
        localContrast += Math.abs(pLA[li] - pLA[i == 0 ? li : li - w]);
        localContrast += Math.abs(pLA[li] - pLA[i == h - 1 ? li : li + w]);

        summands = j == 0 || j == w - 1 ? summands - 1 : summands;
        summands = i == 0 || i == h - 1 ? summands - 1 : summands;

        localContrast /= summands;
        localContrastSum += localContrast;
      }
    }

    return localContrastSum / luminanceArray.length;
  }

  /**
   * Transforms the given color array to linear luminance.
   */
  private double[] colorToLinearLuminance(final int[] colorArray)
  {
    final double[] luminanceArray = new double[colorArray.length];

    for (int i = 0; i < colorArray.length; i++)
    {
      luminanceArray[i] =
        /*.2126 * Math.pow(((colorArray[i] & 0x00FF0000) >>> 16) / 255, 2.2) +
          .7152 * Math.pow(((colorArray[i] & 0x0000FF00) >>> 8) / 255, 2.2) +
          .0722 * Math.pow((colorArray[i] & 0x000000FF) / 255, 2.2);*/
        // The image is already grayscale
        Math.pow((colorArray[i] & 0x000000FF) / 255., 2.2);
    }

    return luminanceArray;
  }

  /**
   * Creates a single superpixel with given size and given start position from
   * the given luminance array.
   */
  private double createSuperPixel(final double[] luminanceArray,
    final int pixelSize,
    final int wStart, final int hStart, final int h)
  {
    double sum = 0.;

    // Simple sum of the luminance
    for (int i = hStart; i < hStart + pixelSize; i++)
    {
      for (int j = wStart; j < wStart + pixelSize; j++)
      {
        sum += luminanceArray[i * h + j];
      }
    }

    // Build average
    return sum / (pixelSize * pixelSize);
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // No phenotype means worst evaluation
    if (individual.getPhenotype() == null)
    {
      return this.getWorstEvaluation();
    }

    // Create grayscale image and extract the color array from it
    final BufferedImage image =
      new GrayscaleFilter().filter(individual.getPhenotype(), null);
    final int[] colorArray =
      ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    // Transform color to luminance
    final double[] luminanceArray = this.colorToLinearLuminance(colorArray);

    // Loop variables
    double[] superPixelArray;
    final int width = image.getWidth();
    final int height = image.getHeight();
    int cWidth = width, cHeight = height;

    // Number of iterations
    final int n = 9;

    double gcf = 0;
    // Super pixel sizes for all iterations
    final int[] superPixelSizes =
      new int[] { 1, 2, 4, 10, 20, 25, 50, 100, 250 };

    // For each iteration, break down the image into fewer, bigger superpixels
    for (int i = 1; i < n; i++)
    {
      superPixelArray =
        this.buildSuperpixelArray(luminanceArray, superPixelSizes[i - 1],
          width, height);
      cWidth = (int) Math.ceil((double) width / superPixelSizes[i - 1]);
      cHeight = (int) Math.ceil((double) height / superPixelSizes[i - 1]);

      gcf += this.weightFactor(i) * this.calculateAverageLocalContrast(
        superPixelArray, cWidth, cHeight);
    }

    return new Double(gcf);
  }

  @Override
  public Double getBestEvaluation()
  {
    return this.optimizationGoal;
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(0.);
  }

  /**
   * Returns the weight factor for the resultion identified by the index.
   * 
   * TODO: reference
   */
  private double weightFactor(final int i)
  {
    return (-.406385 * (i / 9) + .334573) * (i / 9) + .0877526;
  }
}
