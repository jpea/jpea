package de.saviola.jpea.core.evaluate.gui;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.evaluate.JpeaFitnessEvaluator;
import de.saviola.jpea.core.evaluate.gui.individual.CompositeIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.EnlargedIndividualViewer;
import de.saviola.jpea.core.evaluate.gui.individual.IndividualViewer;
import de.saviola.jpea.core.evaluate.gui.views.IndividualView;
import de.saviola.jpea.core.evaluate.individual.IndividualEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.exception.EndOfEvolutionException;
import de.saviola.jpea.core.statistic.StatisticsCollector;

/**
 * GUI for automatic image evaluators and generally everything that is not
 * interactive.
 */
@SuppressWarnings("serial")
public class AutomaticImageEvaluatorGui<G> extends EvaluatorGui
  implements PopulationEvaluator<G, BufferedImage, Double>,
  CompositeIndividualViewer<G, BufferedImage, Double>
{
  private final static Logger logger = LoggerFactory
    .getLogger(AutomaticImageEvaluatorGui.class);

  /**
   * The best evaluation encountered so far.
   */
  private Double bestEvaluation = new Double(Double.NaN);

  /**
   * Current best individual.
   */
  private Individual<G, BufferedImage, Double> bestIndividual;

  /**
   * GUI controls.
   */
  private JButton buttonTogglePause;

  /**
   * The fitness evaluator used to determine the current best individual.
   */
  private final JpeaFitnessEvaluator<Double> fitnessEvaluator;

  /**
   * Current generation number (first generation is 0).
   */
  private int generationNumber = 0;

  /**
   * View in which individuals will be displayed.
   */
  private IndividualView<G, Double> individualView;

  /**
   * The individual viewer showing the current best individual.
   */
  private EnlargedIndividualViewer<G, Double> individualViewer;

  /**
   * The inner evaluator.
   */
  private final PopulationEvaluator<G, BufferedImage, Double> innerEvaluator;

  /**
   * Lock, condition and flag to allow the evolutionary process to be paused.
   */
  private final Lock lock = new ReentrantLock();
  private final Condition lockConditionUnpaused = this.lock.newCondition();
  private boolean paused = false, quit = false;

  public AutomaticImageEvaluatorGui(
    final PopulationEvaluator<G, BufferedImage, Double> evaluator)
  {
    super();

    this.innerEvaluator = evaluator;
    this.fitnessEvaluator = new JpeaFitnessEvaluator<>(
      this.getIndividualEvaluator().getComparator());
  }

  /**
   * When one of the buttons is clicked.
   */
  @Override
  public void actionPerformed(final ActionEvent event)
  {
    // Quit if the user wants to
    if (event.getSource().equals(this.getButtonQuit()))
    {
      // TODO extract into superclass
      this.quit = true;

      // To be able to quit, we need to make sure that the evolution thread
      // is not lazing around
      if (this.paused)
      {
        event.setSource(this.buttonTogglePause);
      }

      this.dispose();
    }
    // Toggle the paused flag and notify paused threads if necessary
    if (event.getSource().equals(this.buttonTogglePause))
    {
      this.paused = !this.paused;

      if (this.paused == false)
      {
        this.lock.lock();
        this.lockConditionUnpaused.signalAll();
        this.lock.unlock();

        this.buttonTogglePause.setText("Pause");
      }
      else
      {
        this.buttonTogglePause.setText("Continue");
      }
    }
  }

  @Override
  public void addIndividualViewer(
    final IndividualViewer<G, BufferedImage, Double> viewer)
  {
    this.individualView.addIndividualViewer(viewer);
  }

  /**
   * Will be called after an individual has been evaluated.
   */
  private void evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    // If the individual has an evaluation
    if (individual.getEvaluation() != null)
    {
      // Check if we found a new best individual
      if (this.fitnessEvaluator.isFitter(individual.getEvaluation(),
        this.bestEvaluation))
      {
        logger.debug("BETTER: {} < {}", this.bestEvaluation,
          individual.getEvaluation());

        this.bestEvaluation = individual.getEvaluation();
        this.bestIndividual = individual;
      }
    }
  }

  /**
   * Will be called when the whole population has to be evaluated.
   */
  @Override
  public void evaluatePopulation(
    final Population<G, BufferedImage, Double> population)
  {
    this.lock.lock();

    // First let's see if the user wants to pause
    while (this.paused)
    {
      try
      {
        this.lockConditionUnpaused.await();
      }
      catch (final InterruptedException e)
      {
        // TODO
        e.printStackTrace();
      }
    }

    this.lock.unlock();

    // Then let's see if the user wants to quit
    if (this.quit)
    {
      this.dispose();

      throw new EndOfEvolutionException();
    }

    // Increase generation number
    this.generationNumber++;

    this.innerEvaluator.evaluatePopulation(population);

    // Reset best evaluation to always show the best image of the current
    // generation
    this.bestEvaluation = new Double(Double.NaN);

    // Post-evaluation checks
    for (final Individual<G, BufferedImage, Double> individual : population
      .getIndividuals())
    {
      this.evaluateIndividual(individual);
    }

    if (this.bestIndividual != null && this.bestIndividual.hasPhenotype())
    {
      this.showIndividual(this.bestIndividual);

      // Save best individual
      this.saveImage(this.bestIndividual);
    }

    // Update label
    SwingUtilities.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        AutomaticImageEvaluatorGui.this.getStatusLabel().setText(
          "Current generation: "
            + AutomaticImageEvaluatorGui.this.generationNumber +
            " | best evaluation (current generation): "
            + (AutomaticImageEvaluatorGui.this.bestEvaluation.isNaN() ? "-"
              : AutomaticImageEvaluatorGui.this.bestEvaluation.toString()));
      }
    });

    this.pack();
  }

  /**
   * Returns the innner individual evaluator.
   */
  @Override
  public IndividualEvaluator<G, BufferedImage, Double> getIndividualEvaluator()
  {
    return this.innerEvaluator.getIndividualEvaluator();
  }

  /**
   * Returns the statistics collector.
   */
  @Override
  public StatisticsCollector<G, BufferedImage, Double> getStatisticsCollector()
  {
    return this.innerEvaluator.getStatisticsCollector();
  }

  /**
   * Initializes the evaluator.
   * 
   * TODO make private?
   */
  public void initialize()
  {
    this.individualView = new IndividualView<>();

    // Setup individual viewer
    this.individualViewer = new EnlargedIndividualViewer<>();
    this.addIndividualViewer(this.individualViewer);

    // Setup the label with the generation number
    this.getStatusLabel().setText(
      "Current generation: " + this.generationNumber);

    // Setup buttons
    this.buttonTogglePause = new JButton("Pause");
    this.buttonTogglePause.addActionListener(this);

    // Setup parent GUI
    this.getControlPanel().add(this.buttonTogglePause);

    this.getViews().addTab("Individual", this.individualView);

    this.showGui();
  }

  /**
   * Saves the phenotype of the given individual.
   * 
   * @TODO rename and save genotype as well or change parameter
   */
  private void saveImage(final Individual<G, BufferedImage, Double> individual)
  {
    try
    {
      // TODO extract
      MarvinImageIO.saveImage(
        new MarvinImage(individual.getPhenotype()),
        "individual_"
          + String.format("%03d", new Integer(this.generationNumber))
          + ".png");
    }
    catch (final Exception e)
    {
      logger.warn("Could not save current best individual: " + e.getMessage());
    }
  }

  @Override
  public void setIndividualEvaluator(
    final IndividualEvaluator<G, BufferedImage, Double> individualEvaluator)
  {
    this.innerEvaluator.setIndividualEvaluator(individualEvaluator);
  }

  @Override
  public void setStatisticsCollector(
    final StatisticsCollector<G, BufferedImage, Double> statistics)
  {
    this.innerEvaluator.setStatisticsCollector(statistics);
  }

  @Override
  public void showIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    this.individualView.showIndividual(individual);
  }
}
