package de.saviola.jpea.core.evaluate.individual;

import java.util.Comparator;

/**
 * Abstract individual evaluator, providing comparator support.
 */
public abstract class AbstractIndividualEvaluator<G, P, V> implements
  IndividualEvaluator<G, P, V>
{
  /**
   * The comparator used to compare fitness values.
   */
  private Comparator<V> comparator;

  /**
   * Returns the comparator.
   */
  @Override
  public Comparator<V> getComparator()
  {
    return this.comparator;
  }

  /**
   * Set the comparator to be used.
   */
  public void setComparator(final Comparator<V> comparator)
  {
    this.comparator = comparator;
  }
}
