package de.saviola.jpea.core.application;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import de.saviola.jpea.core.statistic.StatisticsManager;

/**
 * The Jpea application frame.
 * 
 * The current main task of the Jpea application is to list a number of
 * choosable scenarios and delegate execute to the chosen one afterwards.
 * 
 * @TODO rename? Just Application?
 */
@SuppressWarnings("serial")
public class JpeaApplication extends JFrame
{
  /**
   * The scenario chooser of this application.
   */
  final private ScenarioChooser scenarioChooser = new ScenarioChooser();

  /**
   * Add a new scenario.
   */
  public void addScenario(final Scenario scenario)
  {
    this.scenarioChooser.addScenario(scenario);
  }

  /**
   * Initializes the statistic subsystem by setting the output directory
   * 
   * @TODO make this configurable, and extract it.
   */
  private void initializeStatisticSubsystem()
  {
    StatisticsManager.getInstance().setOutputDirectory("/tmp/jpea/");
  }

  /**
   * Start the application.
   * 
   * Will use the {@link ScenarioChooser} to display all known scenarios.
   */
  public void start() throws Exception
  {
    this.initializeStatisticSubsystem();

    final JFrame frame = this;
    final JPanel mainPanel = new JPanel();

    // Setup the gui
    SwingUtilities.invokeAndWait(new Runnable()
    {
      @Override
      public void run()
      {
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        JpeaApplication.this.scenarioChooser.initialize();
        mainPanel.add(new JLabel("Choose a scenario."));
        mainPanel.add(JpeaApplication.this.scenarioChooser);

        frame.getContentPane().add(mainPanel);

        frame.setTitle("Jpea Scenario Chooser");

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
      }
    });

    // Wait until the user has chosen a scenario.
    try
    {
      this.scenarioChooser.lock.lock();

      while (this.scenarioChooser.getChosenScenario() == null)
      {
        this.scenarioChooser.conditionScenarioChosen.await();
      }
    }
    catch (final InterruptedException e)
    {
      e.printStackTrace();
    }
    finally
    {
      this.scenarioChooser.lock.unlock();
    }

    // Hide and destroy the main frame
    this.setVisible(false);
    this.dispose();

    // And start the chosen scenario.
    try
    {
      this.scenarioChooser.getChosenScenario().start();
    }
    catch (final Exception e)
    {
      e.printStackTrace();
    }
  }
}
