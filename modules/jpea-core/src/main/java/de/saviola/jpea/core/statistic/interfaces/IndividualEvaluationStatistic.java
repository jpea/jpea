package de.saviola.jpea.core.statistic.interfaces;

import de.saviola.jpea.core.Individual;

/**
 * Interface for individual evaluation statistics.
 */
public interface IndividualEvaluationStatistic<G, P, V> extends
  Statistic<G, P, V>
{
  /**
   * To be called after an individual received an evaluation from the 
   * evaluator with the given id.
   */
  public void afterEvaluation(Individual<G, P, V> individual, V evaluation,
    Object evaluatorId);
}
