package de.saviola.jpea.core.evaluate.gui;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;
import java.util.Comparator;

import de.saviola.jpea.core.evaluate.PhenotypeDatabase;

/**
 * Image comparator used in the {@link PhenotypeDatabase}.
 */
public class ImageComparator implements Comparator<BufferedImage>
{

  /**
   * Compares to {@link BufferedImage}s.
   * 
   * Returns 0 when the images are equal, i.e. all pixels have the same value,
   * and otherwise compares the string representation of the images.
   * 
   * @TODO Reference equality checking?
   */
  @Override
  public int compare(final BufferedImage o1, final BufferedImage o2)
  {
    final DataBufferInt db1 = (DataBufferInt) o1.getRaster().getDataBuffer();
    final DataBufferInt db2 = (DataBufferInt) o2.getRaster().getDataBuffer();

    boolean equal = true;

    for (int bank = 0; bank < db1.getNumBanks(); bank++)
    {
      final int[] d1 = db1.getData(bank);
      final int[] d2 = db2.getData(bank);

      if (!Arrays.equals(d1, d2))
      {
        equal = false;
        break;
      }
    }

    // If they're not equal, we need a consistent way to determine which one is
    // "greater" or "smaller". We just use the string representations for that.
    if (!equal)
    {
      return o1.toString().compareTo(o2.toString());
    }

    return 0;
  }

}
