package de.saviola.jpea.core.evaluate.individual;

import java.util.Comparator;

import de.saviola.jpea.core.Individual;

/**
 * Interface for individual evaluators.
 */
public interface IndividualEvaluator<G, P, V>
{
  /**
   * Evaluates the given individual, returning the evaluation.
   * 
   * The individual shall not be changed.
   */
  public V evaluateIndividual(Individual<G, P, V> individual);

  /**
   * Returns the perfect score of this evaluator.
   * 
   * Minimizing evaluators will want to return 0 (or
   * {@link Double#NEGATIVE_INFINITY}) here, maximizing will want
   * to return {@link Double#POSITIVE_INFINITY}, any other double is also
   * valid.
   */
  public V getBestEvaluation();

  /**
   * Returns the comparator for fitness values of this evaluator.
   */
  public Comparator<V> getComparator();

  /**
   * Returns the worst score of this evaluator.
   * 
   * May be infinite.
   */
  public V getWorstEvaluation();
}
