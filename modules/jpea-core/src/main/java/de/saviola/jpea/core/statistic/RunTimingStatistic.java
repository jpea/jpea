package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.statistic.interfaces.RunStatistic;

/**
 * Run timing statistic.
 */
public class RunTimingStatistic<G, P, V> extends TimingStatistic
  implements RunStatistic<G, P, V>
{
  /**
   * Current run.
   */
  private int run = 0;

  @Override
  public void afterRunEnd()
  {
    this.stop();
  }

  @Override
  public void beforeRunStart()
  {
    this.run++;
    this.start(new Integer(this.run));
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "run", "time" };
  }
}
