package de.saviola.jpea.core.evaluate.individual;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;

import org.apache.commons.math3.fitting.PolynomialFitter;
import org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jhlabs.image.GrayscaleFilter;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Fractal dimension evaluator using a box counting algorithm.
 * @param <G>
 */
public class FractalDimensionEvaluator<G> extends
  AbstractIndividualEvaluator<G, BufferedImage, Double>
{
  private final static Logger logger = LoggerFactory
    .getLogger(FractalDimensionEvaluator.class);

  public FractalDimensionEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Returns the calculated fractal dimension of the given image.
   * 
   * This method is an adaption of 
   * <a href="https://github.com/perchrh/ImageJFractalDimension/blob/master/MapFractalCount_.java">
   * this</a> implementation.
   */
  private Double calculateFractalDimension(final BufferedImage image)
  {
    // Fetch data
    final int width = image.getWidth();
    final int height = image.getHeight();

    final int maxBox = Math.max(width, height) / 8;
    final int minBox = 2;

    final boolean subGraph = true;
    final int numOffsets = 1;

    final int[] pixels =
      ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

    final double zScale = 1.;

    double min = Double.MAX_VALUE;
    double value;
    for (int x = 0; x < width; x++)
    {
      for (int y = 0; y < height; y++)
      {
        value = pixels[y + x * height] & 0x000000FF;
        if (value < min)
        {
          min = value;
        }
      }
    }

    // Create variables we need and set them
    int bestCount;
    int count = 0;
    final ArrayList<Double> xList = new ArrayList<>();
    final ArrayList<Double> yList = new ArrayList<>();
    int iMax, jMax;
    //final int xPos, yPos;
    int xGrid, yGrid;
    int xStart, yStart;
    int xEnd, yEnd;
    int increment;
    double zValue;
    double boxZMin, boxZMax;

    for (int boxSize = maxBox; boxSize >= minBox; boxSize -= 2)
    {

      bestCount = Integer.MAX_VALUE; // Init count for this boxSize
      increment = Math.max(1, boxSize / numOffsets);

      for (int gridOffsetX = 0; gridOffsetX < boxSize
        && gridOffsetX < width; gridOffsetX += increment)
      {

        for (int gridOffsetY = 0; gridOffsetY < boxSize
          && gridOffsetY < height; gridOffsetY += increment)
        {

          count = 0;

          iMax = width + gridOffsetX;
          jMax = height + gridOffsetY;

          // Iterate over box-grid
          for (int i = 0; i <= iMax; i += boxSize)
          {
            xGrid = -gridOffsetX + i;
            for (int j = 0; j <= jMax; j += boxSize)
            {
              yGrid = -gridOffsetY + j;

              xStart = 0;
              if (xGrid < 0)
              {
                xStart = -xGrid;
              }
              if (boxSize + xGrid >= width)
              {
                xEnd = Math.min(width, width - xGrid);
              }
              else
              {
                xEnd = boxSize;
              }

              yStart = 0;
              if (yGrid < 0)
              {
                yStart = -yGrid;
              }
              yEnd = boxSize;
              if (boxSize + yGrid >= height)
              {
                yEnd = Math.min(height, height - yGrid);
              }

              boxZMin = Float.POSITIVE_INFINITY;
              boxZMax = Float.NEGATIVE_INFINITY;

              // Inspect box
              for (int x = xStart; x < xEnd; x++)
              {
                //xPos = x + xGrid;

                for (int y = yStart; y < yEnd; y++)
                {
                  //yPos = y + yGrid;

                  zValue = zScale * (pixels[y + x * yEnd] & 0x000000FF);

                  if (zValue < boxZMin)
                  {
                    boxZMin = zValue;
                  }
                  if (zValue > boxZMax)
                  {
                    boxZMax = zValue;
                  }
                }
              }

              int boxes = 0;

              // If a box is entirely outside image edges,
              // ignore this box
              if (boxZMax == Float.NEGATIVE_INFINITY)
              {
                continue;
              }
              // Else, calculate box count of the column with
              // base
              // {x=xStart..xEnd,y=yStart..yEnd} based on the
              // SDBC way.
              // The SDBC algorithm is described in
              // "Two algorithms to estimate fractal dimension
              // of gray level images" by Wen-Shiung Chen et.al.
              // Published in "Optical Engineering", Vol 42.
              // No. 8, August 2003.

              if (subGraph)
              {
                boxes = 1 + (int) ((boxZMax - min + 1) / boxSize);
              }
              else
              {
                boxes = 1 + (int) ((boxZMax - boxZMin + 1) / boxSize);

              }
              count += boxes;
            }
          }
          if (count < bestCount)
          {
            bestCount = count;
          }
        }
      }

      xList.add(new Double((double) boxSize / (double) width));
      yList.add(new Double(bestCount));
    }

    if (xList.size() == 0)
    {
      logger.error("Error: No boxes! Make sure that starting and ending box " +
        "size and reduction rate allow for at least one box size to exist!");
      return this.getWorstEvaluation();
    }

    final PolynomialFitter fitter =
      new PolynomialFitter(new LevenbergMarquardtOptimizer());

    final double[] boxSizes = new double[xList.size()];
    final double[] boxCounts = new double[yList.size()];
    for (int i = 0; i < boxSizes.length; i++)
    {
      boxSizes[i] = -Math.log(xList.get(i).doubleValue());
      boxCounts[i] = Math.log(yList.get(i).doubleValue());

      fitter.addObservedPoint(boxSizes[i], boxCounts[i]);
    }

    final double[] best = fitter.fit(new double[] { -1., 1.5 });

    final Double dimension = new Double(best[1]);

    return dimension;
  }

  @Override
  public Double evaluateIndividual(
    final Individual<G, BufferedImage, Double> individual)
  {
    final BufferedImage image = new GrayscaleFilter().filter(
      individual.getPhenotype(), null);

    return this.calculateFractalDimension(image);
  }

  /**
   * Returns the best evaluation, which is 1.35 (target fractal dimension).
   */
  @Override
  public Double getBestEvaluation()
  {
    return new Double(1.35);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(0.);
  }

}
