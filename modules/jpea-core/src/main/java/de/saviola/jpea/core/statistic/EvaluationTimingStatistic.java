package de.saviola.jpea.core.statistic;

import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.interfaces.PopulationEvaluationStatistic;

/**
 * Evaluation timing statistic.
 */
public class EvaluationTimingStatistic<G, P, V> extends
  TimingStatistic
  implements PopulationEvaluationStatistic<G, P, V>
{
  @Override
  public void afterEvaluation(final Population<G, P, V> population)
  {
    this.stop();
  }

  @Override
  public void beforeEvaluation(final Population<G, P, V> population)
  {
    this.incrementGeneration();
    this.start(new Integer(this.getGeneration()));
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "generation", "evaluation time" };
  }

}
