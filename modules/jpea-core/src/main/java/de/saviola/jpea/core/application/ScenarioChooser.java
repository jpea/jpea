package de.saviola.jpea.core.application;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * {@link JPanel} that uses a {@link JComboBox} to show a number of scenarios.
 */
@SuppressWarnings("serial")
public class ScenarioChooser extends JPanel implements ActionListener
{
  /**
   * Lock and condition to determine when a scenario was chosen.
   */
  public Lock lock = new ReentrantLock();
  public Condition conditionScenarioChosen = lock.newCondition();

  /**
   * Chosen scenario or null of none was chosen yet.
   */
  private Scenario chosenScenario;

  /**
   * List of known scenarios.
   */
  private List<Scenario> scenarios = new ArrayList<>();

  /**
   * GUI elements, namely a combo box with a list of scenarios and a submit
   * button.
   */
  private JComboBox<String> scenarioComboBox;
  private JButton buttonSubmit;

  public ScenarioChooser()
  {
    super();
  }

  /**
   * Helper method to notify other threads that the scenario was chosen.
   */
  private void notifyScenarioChosen()
  {
    this.lock.lock();

    this.conditionScenarioChosen.signalAll();

    this.lock.unlock();
  }

  /**
   * Add a new scenario.
   */
  public void addScenario(Scenario scenario)
  {
    this.scenarios.add(scenario);
  }

  /**
   * Initialize the scenario chooser.
   */
  public void initialize()
  {
    String[] scenarioTitles = new String[this.scenarios.size()];

    // Build a list of scenario titles
    for (int i = 0; i < this.scenarios.size(); i++)
    {
      scenarioTitles[i] = this.scenarios.get(i).getTitle();
    }

    // Create a combo box using this titles
    this.scenarioComboBox = new JComboBox<>(scenarioTitles);

    // Setup the submit button
    this.buttonSubmit = new JButton("Start");
    this.buttonSubmit.addActionListener(this);

    this.add(this.scenarioComboBox);
    this.add(this.buttonSubmit);
    this.setVisible(true);
  }

  /**
   * When the submit button was clicked, set the chosen scenario and notify
   * others of it.
   */
  @Override
  public void actionPerformed(ActionEvent event)
  {
    if (event.getSource().equals(this.buttonSubmit))
    {
      this.chosenScenario =
        this.scenarios.get(this.scenarioComboBox.getSelectedIndex());

      this.notifyScenarioChosen();
    }
  }

  /**
   * Returns the chosen scenario or null, if none was chosen yet.
   */
  public Scenario getChosenScenario()
  {
    return this.chosenScenario;
  }
}
