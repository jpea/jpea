package de.saviola.jpea.jgap.gp.problem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.GPProblem;
import org.jgap.gp.function.ADF;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;

import de.saviola.jpea.jgap.gp.function.SimpleArithmeticPrimitiveSet;

/**
 * Arithmetic problem supporting ADFs.
 */
public class AdfArithmeticProblem extends GPProblem
{
  public AdfArithmeticProblem(final GPConfiguration config)
    throws InvalidConfigurationException
  {
    super(config);
  }

  @Override
  public GPGenotype create() throws InvalidConfigurationException
  {
    final Collection<CommandGene> functionSet =
      new SimpleArithmeticPrimitiveSet().create(this.getGPConfiguration());

    final Collection<CommandGene> functionSetAdf =
      new ArrayList<>(functionSet);

    functionSet.add(new ADF(this.getGPConfiguration(), 1, 2));

    final Class<?>[] types = {
      Vector.class,
      Double.class, };
    final Class<?>[][] argTypes = {
      {},
      { Double.class, Double.class },
    };

    // Define the commands and terminals the GP is allowed to use.
    final CommandGene[][] nodeSets = {
      functionSet.toArray(new CommandGene[] {}),
      functionSetAdf.toArray(new CommandGene[] {}),
    };

    // Create genotype with initial population.
    // Allow max. 500 nodes within one program.
    // TODO make max node number configurable
    return GPGenotype.randomInitialGenotype(this.getGPConfiguration(), types,
      argTypes, nodeSets,
      500, true);
  }
}
