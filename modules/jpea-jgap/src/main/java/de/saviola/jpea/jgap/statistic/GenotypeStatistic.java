package de.saviola.jpea.jgap.statistic;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.statistic.GenerationStatistic;
import de.saviola.jpea.core.statistic.interfaces.PopulationEvaluationStatistic;

/**
 * Genotype statisitc, keeping track of tree depth and node count.
 */
public class GenotypeStatistic<P, V> extends GenerationStatistic
  implements PopulationEvaluationStatistic<IGPProgram, P, V>
{
  @Override
  public void afterEvaluation(final Population<IGPProgram, P, V> population)
  {
    for (final Individual<IGPProgram, P, V> individual : population
      .getIndividuals())
    {
      this.addRow(new Integer(System.identityHashCode(individual)),
        new Integer(this.getGeneration()),
        new Integer(this.getProgramDepth(individual.getGenotype())),
        new Integer(this.getProgramNodeCount(individual.getGenotype())));
    }
  }

  @Override
  public void beforeEvaluation(final Population<IGPProgram, P, V> population)
  {
    this.incrementGeneration();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "id", "generation", "depth", "nodeCount" };
  }

  /**
   * Returns the program depth of the given program.
   * 
   * TODO make this work for programs with several chromosomes?
   */
  private int getProgramDepth(final IGPProgram program)
  {
    // The depth of the program is the depth of its first (and only) chromosome
    return program.getChromosome(0).getDepth(0);
  }

  /**
   * Returns the node count of the given program.
   */
  private int getProgramNodeCount(final IGPProgram program)
  {
    return program.getChromosome(0).size();
  }
}