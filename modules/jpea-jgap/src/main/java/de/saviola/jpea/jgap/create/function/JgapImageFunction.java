package de.saviola.jpea.jgap.create.function;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.function.ImageFunction;

/**
 * JGAP image function, executing an {@link IGPProgram} instance.
 * @param <V>
 */
public abstract class JgapImageFunction<V> implements ImageFunction<V>
{
  /**
   * Empty args array.
   * 
   * @see #evaluateAt(double, double)
   */
  protected static final Object[] noArgs = new Object[0];

  /**
   * The underlying program which will be executed.
   */
  protected IGPProgram program;

  /**
   * Takes the program which will be executed.
   */
  public JgapImageFunction(final IGPProgram program)
  {
    this.setProgram(program);
  }

  /**
   * Stores the given program and extracts the variables from it.
   */
  private void setProgram(final IGPProgram jgapProgram)
  {
    this.program = jgapProgram;
  }
}
