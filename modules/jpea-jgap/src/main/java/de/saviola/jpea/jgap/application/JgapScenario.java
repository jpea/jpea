package de.saviola.jpea.jgap.application;

import javax.swing.SwingUtilities;

import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.application.CompositeGuiConfigurator;
import de.saviola.jpea.core.application.EvolutionEngine;
import de.saviola.jpea.core.application.Scenario;
import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.exception.EndOfEvolutionException;
import de.saviola.jpea.core.statistic.StatisticsCollector;
import de.saviola.jpea.core.statistic.StatisticsManager;
import de.saviola.jpea.jgap.JgapDriver;
import de.saviola.jpea.jgap.application.config.StatisticsConfigurator;
import de.saviola.jpea.jgap.evaluate.JgapDoubleFitnessEvaluator;
import de.saviola.jpea.jgap.statistic.GenotypeStatistic;

/**
 * Abstract JGAP scenario providing setup and configuration directives.
 */
public abstract class JgapScenario<P> implements Scenario
{
  /**
   * Compositve configurator.
   */
  private final CompositeGuiConfigurator configurator =
    new CompositeGuiConfigurator();

  /**
   * JGAP driver.
   */
  private final JgapDriver<P> driver = new JgapDriver<>();

  /**
   * Starts the configuration process.
   */
  private void configure() throws Exception
  {
    // Initialize GUI
    SwingUtilities.invokeAndWait(new Runnable()
    {
      @Override
      public void run()
      {
        JgapScenario.this.configurator.initialize();
      }
    });

    // This will only return when the configuration has finished
    this.configurator.updateConfiguration();
  }

  /**
   * Creates the phenotype creator.
   */
  public abstract PhenotypeCreator<IGPProgram, P> createPhenotypeCreator()
    throws Exception;

  /**
   * Creates the population evaluator.
   */
  public abstract PopulationEvaluator<IGPProgram, P, Double> createPopulationEvaluator()
    throws Exception;

  /**
   * Creates the GPProblem.
   */
  public abstract GPProblem createProblem() throws Exception;

  /**
   * Returns the configurator.
   */
  public CompositeGuiConfigurator getConfigurator()
  {
    return this.configurator;
  }

  /**
   * Returns the {@link JgapDriver}.
   */
  public JgapDriver<P> getDriver()
  {
    return this.driver;
  }

  /**
   * Setup hook for configurators.
   */
  public abstract void setUpConfigurators() throws Exception;

  /**
   * Sets up the {@link JgapDriver}.
   */
  public void setUpDriver() throws Exception
  {
    // Initialize jgap driver
    this.driver.initialize();

    // Create GP problem
    this.driver.setProblem(this.createProblem());

    // Generation count
    // TODO extract into the JgapDriverConfigurator
    this.driver.getConfig().storeInMemory("generations", new Integer(50));
  }

  @Override
  public void start() throws Exception
  {
    this.setUpDriver();

    this.setUpConfigurators();

    this.configure();

    // Create evaluator
    final PopulationEvaluator<IGPProgram, P, Double> evaluatorGui =
      this.createPopulationEvaluator();

    // Statistics collector
    final StatisticsCollector<IGPProgram, P, Double> statistics =
      new StatisticsConfigurator<IGPProgram, P, Double>()
        .createStatisticsCollector();

    // Register statistics collector in evaluator
    evaluatorGui.setStatisticsCollector(statistics);

    // TODO move to configurator?
    final GenotypeStatistic<P, Double> genotypeStatistic =
      new GenotypeStatistic<>();
    genotypeStatistic.setExporter(StatisticsManager.getInstance()
      .createDataExporter("genotypes"));

    // Add genotype statistic
    statistics.addStatistic(genotypeStatistic);

    // Set the fitness evaluator and start the evolution
    this.driver.getConfig().setGPFitnessEvaluator(
      new JgapDoubleFitnessEvaluator(evaluatorGui.getIndividualEvaluator()
        .getComparator()));

    try
    {
      statistics.beforeRunStart();
      this.driver.startEvolution(new EvolutionEngine<>(this
        .createPhenotypeCreator(),
        evaluatorGui,
        statistics));
    }
    catch (final EndOfEvolutionException e)
    {
      // Evolution has ended (prematurely) through termination by the user
    }
    finally
    {
      statistics.afterRunEnd();
    }
  }
}
