package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.evaluate.FitnessTransformation;
import de.saviola.jpea.core.evaluate.individual.GlobalContrastFactorEvaluator;
import de.saviola.jpea.core.evaluate.individual.ImageComplexityEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.evaluate.population.WeightedProductPopulationEvaluator;
import de.saviola.jpea.jgap.evaluate.individual.NodeCountEvaluator;

/**
 * Image complexity plus GCF scenario (also using bloat control).
 */
public class ImageComplexityGcfScenario extends ImageComplexityBCScenario
{
  @Override
  public PopulationEvaluator<IGPProgram, BufferedImage, Double> createPopulationEvaluator()
    throws Exception
  {
    // Create the evaluator(s)
    final WeightedProductPopulationEvaluator<IGPProgram, BufferedImage> evaluator =
      new WeightedProductPopulationEvaluator<>();
    evaluator.addIndividualEvaluator(
      new ImageComplexityEvaluator<IGPProgram>(),
      new FitnessTransformation(1., .001, 0));
    evaluator.addIndividualEvaluator(
      new GlobalContrastFactorEvaluator<IGPProgram>(100.),
      new FitnessTransformation());
    evaluator.addIndividualEvaluator(
      new NodeCountEvaluator<BufferedImage>(), new FitnessTransformation(-1,
        .001, 1.));

    return super.createEvaluatorGui(evaluator);
  }

  @Override
  public String getTitle()
  {
    return "Image Complexity + GCF (bloat control)";
  }
}
