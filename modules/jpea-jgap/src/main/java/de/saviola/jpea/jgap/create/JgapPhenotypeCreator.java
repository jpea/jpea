package de.saviola.jpea.jgap.create;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.create.ConcurrentPhenotypeCreator;

/**
 * JGAP phenotype creator for genotypes which directly return the phenotype
 * upon execution.
 */
public class JgapPhenotypeCreator<P> extends
  ConcurrentPhenotypeCreator<IGPProgram, P>
{
  /**
   * TODO fix warning
   */
  @SuppressWarnings("unchecked")
  @Override
  public void createForIndividual(final Individual<IGPProgram, P, ?> individual)
  {
    individual.setPhenotype((P) individual.getGenotype().execute_object(0,
      new Object[0]));
  }

}
