package de.saviola.jpea.jgap.application;

import de.saviola.jpea.core.application.JpeaApplication;

/**
 * Helper class that contains the main method and starts up the Jpea
 * application, adding all JGAP-based scenarios to it beforehand.
 * 
 * TODO solve this more dynamically
 */
public class ApplicationStarter
{
  public static void main(final String[] args) throws Exception
  {
    final JpeaApplication application = new JpeaApplication();
    application.addScenario(new InteractiveScenario());
    application.addScenario(new FractalScenario());
    application.addScenario(new FractalBCScenario());
    application.addScenario(new FractalDimensionScenario());
    application.addScenario(new GcfScenario());
    application.addScenario(new GcfBCScenario());
    application.addScenario(new ImageComplexityScenario());
    application.addScenario(new ImageComplexityBCScenario());
    application.addScenario(new ImageComplexityGcfScenario());
    application.start();

    // TODO fix termination bug
    System.exit(0);
  }
}
