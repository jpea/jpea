package de.saviola.jpea.jgap.gp;

import org.jgap.RandomGenerator;

/**
 * Quick & Dirty global access to the {@link RandomGenerator} to allow seeding.
 */
public class RNG
{
  private static RandomGenerator rng;

  public static RandomGenerator getRNG()
  {
    return RNG.rng;
  }

  public static void setRNG(final RandomGenerator rng)
  {
    RNG.rng = rng;
  }

  private RNG()
  {
    // Nothing to do here
  }
}
