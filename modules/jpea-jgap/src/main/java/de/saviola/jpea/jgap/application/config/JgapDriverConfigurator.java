package de.saviola.jpea.jgap.application.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jgap.RandomGenerator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.impl.StockRandomGenerator;

import de.saviola.jpea.core.application.Configurator;
import de.saviola.jpea.jgap.JgapDriver;
import de.saviola.jpea.jgap.gp.RNG;

/**
 * Graphical {@link JgapDriver} configurator.
 */
@SuppressWarnings("serial")
public class JgapDriverConfigurator extends JPanel implements Configurator
{
  /**
   * The driver that is being configured.
   */
  private final JgapDriver<?> driver;

  private JTextField seedTextField;

  /**
   * All the sliders.
   */
  private JSlider sliderFunctionProbability;
  private JSlider sliderNewIndividualsPercent;
  private JSlider sliderTreeDepthMaximum;
  private JSlider sliderTreeDepthMinimum;
  private JSlider sliderTreeNodesMaximum;

  /**
   * Text field and label for population size.
   */
  private JTextField fieldPopulationSize;

  /**
   * Map of slider values.
   */
  private final Map<String, Vector<Integer>> sliderValues = new HashMap<>();

  public JgapDriverConfigurator(final JgapDriver<?> driver)
  {
    this.driver = driver;
  }

  /**
   * Helper method to create a slider from a given value vector.
   * 
   * Values are fed to the {@link JSlider} constructor in the given order.
   */
  private JSlider createSlider(final Vector<Integer> values)
  {
    return new JSlider(
      values.get(0).intValue(),
      values.get(1).intValue(),
      values.get(2).intValue());
  }

  /**
   * Enable the function probability slider.
   */
  public void enableFunctionProbability(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("functionProbability", min, max, initial);
  }

  /**
   * Enable the slider which controls the percentage of new individuals.
   */
  public void enableNewIndividualsPercent(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("newIndividualsPercent", min, max, initial);
  }

  /**
   * Enable the slider which controls the population size.
   */
  public void enablePopulationSize(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("populationSize", min, max, initial);
  }

  /**
   * Enable the slider which controls the maximum initial tree depth.
   */
  public void enableTreeDepthMaximum(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("treeDepthMaximum", min, max, initial);
  }

  /**
   * Enable the slider which controls the minimum initial tree depth.
   */
  public void enableTreeDepthMinimum(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("treeDepthMinimum", min, max, initial);
  }

  /**
   * Enable the slider which controls the mamimum overall tree nodes.
   */
  public void enableTreeNodesMaximum(final int min, final int max,
    final int initial)
  {
    this.putSliderValues("treeNodesMaximum", min, max, initial);
  }

  /**
   * Helper method which returns the percentage value of the given slider.
   */
  private double getSliderPercentValue(final JSlider slider)
  {
    return slider.getValue() / 100.;
  }

  /**
   * Initializes the configurator.
   */
  @Override
  public void initialize()
  {
    // Abort if not on EDT
    if (!SwingUtilities.isEventDispatchThread())
    {
      throw new RuntimeException("GUI modification must happen on the EDT!");
    }

    // @formatter:off
    final Vector<Integer>
      functionProbability = this.sliderValues.get("functionProbability"),
      newIndividualsPercent = this.sliderValues.get("newIndividualsPercent"),
      populationSize = this.sliderValues.get("populationSize"),
      treeDepthMaximum = this.sliderValues.get("treeDepthMaximum"),
      treeDepthMinimum = this.sliderValues.get("treeDepthMinimum"),
      treeNodesMaximum = this.sliderValues.get("treeNodesMaximum");
    // @formatter:on

    // Now we walk through all sliders and check if they're enabled.
    // And if they are, we set 'em up with the correct values and label

    if (functionProbability != null)
    {
      this.sliderFunctionProbability = this.createSlider(functionProbability);

      this.setUpSlider(this.sliderFunctionProbability,
        "Probability of functions being chosen as crossover points.");
    }

    if (newIndividualsPercent != null)
    {
      this.sliderNewIndividualsPercent =
        this.createSlider(newIndividualsPercent);

      this.setUpSlider(this.sliderNewIndividualsPercent,
        "Percentage of new individuals in each new generation.");
    }

    if (populationSize != null)
    {
      this.fieldPopulationSize =
        new JTextField(String.valueOf(populationSize.get(2)));
      final JLabel labelPopulationSize = new JLabel("Population size.");
      this.add(labelPopulationSize);
      this.add(this.fieldPopulationSize);
    }

    if (treeDepthMaximum != null)
    {
      this.sliderTreeDepthMaximum = this.createSlider(treeDepthMaximum);

      this.setUpSlider(this.sliderTreeDepthMaximum,
        "Maximum initial tree depth.");
    }

    if (treeDepthMinimum != null)
    {
      this.sliderTreeDepthMinimum = this.createSlider(treeDepthMinimum);

      this.setUpSlider(this.sliderTreeDepthMinimum,
        "Minimum initial tree depth.");
    }

    if (treeNodesMaximum != null)
    {
      this.sliderTreeNodesMaximum = this.createSlider(treeNodesMaximum);

      this.setUpSlider(this.sliderTreeNodesMaximum,
        "Maximum overall nodes in a tree.");
    }

    // Seed
    // TODO fix seeding
    this.seedTextField = new JTextField();
    //this.add(new JLabel("Evolution seed (leave empty for random seed):"));
    //this.add(this.seedTextField);

    // Set up panel
    this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    this.setBorder(BorderFactory.createTitledBorder("JGAP configuration"));
    this.setVisible(true);
  }

  /**
   * Helper method to put slider values into our map.
   */
  private void putSliderValues(final String slider, final int min,
    final int max, final int initial)
  {
    this.sliderValues.put(slider,
      new Vector<>(Arrays.asList(new Integer[] {
        new Integer(min),
        new Integer(max),
        new Integer(initial)
      })));
  }

  /**
   * Helper method to set up the given slider.
   */
  private void setUpSlider(final JSlider slider, final String label)
  {
    slider.setMajorTickSpacing((slider.getMaximum() - slider.getMinimum()) / 4);
    slider.setPaintTicks(true);
    slider.setPaintLabels(true);

    final JLabel sliderLabel = new JLabel(label);

    this.add(sliderLabel);
    this.add(slider);
  }

  /**
   * Commits the slider values into the JGAP configuration.
   */
  @Override
  public void updateConfiguration() throws Exception
  {
    final GPConfiguration config = this.driver.getConfig();

    if (this.sliderFunctionProbability != null)
    {
      config.setFunctionProb(this
        .getSliderPercentValue(this.sliderFunctionProbability));
    }

    if (this.sliderTreeDepthMinimum != null)
    {
      config.setMinInitDepth(this.sliderTreeDepthMinimum.getValue());
    }

    if (this.sliderTreeDepthMaximum != null)
    {
      config.setMaxInitDepth(this.sliderTreeDepthMaximum.getValue());
    }

    if (this.sliderNewIndividualsPercent != null)
    {
      config.setNewChromsPercent(this
        .getSliderPercentValue(this.sliderNewIndividualsPercent));
    }

    if (this.fieldPopulationSize != null)
    {
      int populationSize =
        this.sliderValues.get("populationSize").get(2).intValue();

      try
      {
        populationSize =
          Integer.valueOf(this.fieldPopulationSize.getText()).intValue();
      }
      catch (final Exception e)
      {
        // Number format exception
      }

      config.setPopulationSize(populationSize);
    }

    // Set the seed
    if (!this.seedTextField.getText().equals(""))
    {
      final String seedString = this.seedTextField.getText();
      long seed;

      try
      {
        seed = Long.decode(seedString).longValue();
      }
      catch (final NumberFormatException e)
      {
        seed = seedString.hashCode();
      }

      final Random rng = new StockRandomGenerator();
      rng.setSeed(seed);

      config.setRandomGenerator((RandomGenerator) rng);
    }

    RNG.setRNG(config.getRandomGenerator());
  }
}
