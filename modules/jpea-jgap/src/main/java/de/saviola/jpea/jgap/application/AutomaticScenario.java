package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.core.create.function.VectorFunctionImageCreator;
import de.saviola.jpea.core.evaluate.gui.AutomaticImageEvaluatorGui;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.jgap.application.config.JgapDriverConfigurator;
import de.saviola.jpea.jgap.create.function.JgapArithmeticImageCreator;
import de.saviola.jpea.jgap.evaluate.gui.GenotypeViewer;
import de.saviola.jpea.jgap.gp.problem.SimpleArithmeticProblem;

/**
 * Scenario which faciliates the {@link AutomaticImageEvaluatorGui} for image
 * evaluation.
 * 
 * TODO update documentation
 */
public abstract class AutomaticScenario extends JgapScenario<BufferedImage>
{
  public PopulationEvaluator<IGPProgram, BufferedImage, Double> createEvaluatorGui(
    final PopulationEvaluator<IGPProgram, BufferedImage, Double> evaluator)
    throws Exception
  {
    final AutomaticImageEvaluatorGui<IGPProgram> evaluatorGui =
      new AutomaticImageEvaluatorGui<>(evaluator);

    // Initialize the evaluator GUI
    SwingUtilities.invokeAndWait(new Runnable()
    {
      @Override
      public void run()
      {
        evaluatorGui.initialize();
        evaluatorGui
          .addIndividualViewer(new GenotypeViewer<BufferedImage, Double>());
      }
    });

    return evaluatorGui;
  }

  @Override
  public PhenotypeCreator<IGPProgram, BufferedImage> createPhenotypeCreator()
    throws Exception
  {
    // Create the phenotype creator
    return new JgapArithmeticImageCreator<>(new VectorFunctionImageCreator(500,
      500));
  }

  @Override
  public GPProblem createProblem() throws Exception
  {
    return new SimpleArithmeticProblem(this.getDriver().getConfig());
  }

  @Override
  public String getDescription()
  {
    return this.getTitle();
  }

  @Override
  public void setUpConfigurators() throws Exception
  {
    // Create the driver configurator
    final JgapDriverConfigurator jgapDriverConfigurator =
      new JgapDriverConfigurator(this.getDriver());

    // Set up the configurator
    jgapDriverConfigurator.enableFunctionProbability(50, 90, 90);
    jgapDriverConfigurator.enableNewIndividualsPercent(10, 50, 30);
    jgapDriverConfigurator.enablePopulationSize(10, 410, 50);
    jgapDriverConfigurator.enableTreeDepthMaximum(4, 20, 7);
    jgapDriverConfigurator.enableTreeDepthMinimum(2, 10, 2);

    // Create and set up composite configurator
    this.getConfigurator().addConfigurator(jgapDriverConfigurator);
  }
}
