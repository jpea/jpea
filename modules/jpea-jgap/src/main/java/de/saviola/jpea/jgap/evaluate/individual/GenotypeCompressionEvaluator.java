package de.saviola.jpea.jgap.evaluate.individual;

import java.io.UnsupportedEncodingException;
import java.util.zip.Deflater;

import org.jgap.gp.IGPProgram;
import org.jgap.gp.impl.GPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.AbstractIndividualEvaluator;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Genotype compression evaluator.
 * 
 * Compressed the string representation of the genotype and uses the
 * compression ratio as evaluation measure.
 */
public class GenotypeCompressionEvaluator<P> extends
  AbstractIndividualEvaluator<IGPProgram, P, Double>
{
  public GenotypeCompressionEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * Calculate the compressed length of the given genotype's string
   * representation.
   */
  private Double calculateCompressedLength(final IGPProgram program)
  {
    try
    {
      // Encode a String into bytes
      final String inputString = ((GPProgram) program).toString(0);
      final byte[] input = inputString.getBytes("UTF-8");

      // Compress the bytes
      final byte[] output = new byte[inputString.length()];
      final Deflater compresser = new Deflater();
      compresser.setInput(input);
      compresser.finish();
      final int lengthCompressed = compresser.deflate(output);

      return new Double(lengthCompressed);
    }
    catch (final UnsupportedEncodingException enc)
    {
      // TODO handle
      enc.printStackTrace();
    }

    return this.getWorstEvaluation();
  }

  @Override
  public Double evaluateIndividual(
    final Individual<IGPProgram, P, Double> individual)
  {
    return this.calculateCompressedLength(individual.getGenotype());
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(0.);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }

}
