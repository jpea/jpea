package de.saviola.jpea.jgap.application.config;

import de.saviola.jpea.core.statistic.DefaultEvaluationStatistic;
import de.saviola.jpea.core.statistic.DefectStatistic;
import de.saviola.jpea.core.statistic.EvaluationTimingStatistic;
import de.saviola.jpea.core.statistic.MultiEvaluationStatistic;
import de.saviola.jpea.core.statistic.PhenotypeCreationTimingStatistic;
import de.saviola.jpea.core.statistic.RunTimingStatistic;
import de.saviola.jpea.core.statistic.StatisticsCollector;
import de.saviola.jpea.core.statistic.StatisticsManager;

/**
 * Configures the statistics for an evolutionary run.
 * @param <G> Genotype of the individuals.
 * @param <P> Phenotype of the individuals.
 * @param <V> Evaluation type of the individuals.
 */
public class StatisticsConfigurator<G, P, V>
{
  /**
   * Flag: Whether evaluation statistics should be enabled.
   */
  private boolean evaluationStatisticsEnabled = true;

  /**
   * Flag: Whether timing statistics should be enabled.
   */
  private boolean timingStatisticsEnabled = true;

  /**
   * Create the statistics collector according to above flags.
   */
  public StatisticsCollector<G, P, V> createStatisticsCollector()
  {
    final StatisticsCollector<G, P, V> collector = new StatisticsCollector<>();

    if (this.timingStatisticsEnabled)
    {
      // Run timing statistics
      final RunTimingStatistic<G, P, V> runTiming = new RunTimingStatistic<>();
      runTiming.setExporter(StatisticsManager.getInstance()
        .createDataExporter("run_times"));

      // Creation statistics
      final PhenotypeCreationTimingStatistic<G, P, V> creationTiming =
        new PhenotypeCreationTimingStatistic<>();
      creationTiming.setExporter(StatisticsManager.getInstance()
        .createDataExporter("creation_times"));

      // Evaluation Statistics
      final EvaluationTimingStatistic<G, P, V> evaluationTiming =
        new EvaluationTimingStatistic<>();
      evaluationTiming.setExporter(StatisticsManager.getInstance()
        .createDataExporter("evaluation_times"));

      // Add to collector
      collector.addStatistic(runTiming);
      collector.addStatistic(creationTiming);
      collector.addStatistic(evaluationTiming);
    }

    if (this.evaluationStatisticsEnabled)
    {
      final DefaultEvaluationStatistic<G, P, V> evaluationStatistic =
        new DefaultEvaluationStatistic<>();
      evaluationStatistic.setExporter(StatisticsManager.getInstance()
        .createDataExporter("evaluation_values_cumulative"));

      final MultiEvaluationStatistic<G, P, V> evaluationStatisticIndividual =
        new MultiEvaluationStatistic<>();
      evaluationStatisticIndividual.setExporter(StatisticsManager.getInstance()
        .createDataExporter("evaluation_values_individual"));

      final DefectStatistic<G, P, V> defectStatistic = new DefectStatistic<>();
      defectStatistic.setExporter(StatisticsManager.getInstance()
        .createDataExporter("phenotype_creation_defects"));

      collector.addStatistic(evaluationStatistic);
      collector.addStatistic(evaluationStatisticIndividual);
      collector.addStatistic(defectStatistic);
    }

    return collector;
  }

  /**
   * Sets the flag {@link #evaluationStatisticsEnabled}.
   */
  public void setEvaluationStatisticsEnabled(final boolean flag)
  {
    this.evaluationStatisticsEnabled = flag;
  }

  /**
   * Sets the flag {@link #timingStatisticsEnabled}.
   */
  public void setTimingStatisticsEnabled(final boolean flag)
  {
    this.timingStatisticsEnabled = flag;
  }
}
