package de.saviola.jpea.jgap.create.function;

import java.awt.image.BufferedImage;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.create.function.FunctionImageCreator;
import de.saviola.jpea.core.create.function.ImageFunction;

/**
 * Arithmetic image creator using image functions.
 */
public class JgapArithmeticImageCreator<T> extends
  JgapImageCreator<ImageFunction<T>>
{
  public JgapArithmeticImageCreator(
    final FunctionImageCreator<ImageFunction<T>, BufferedImage> imageCreator)
  {
    super(imageCreator);
  }

  /**
   * Creates the phenotype for the given individual.
   */
  @Override
  public void createForIndividual(
    final Individual<IGPProgram, BufferedImage, ?> individual)
  {
    // Bail out if the phenotype has already been created
    if (individual.getPhenotype() != null)
    {
      return;
    }

    // Wrap the JGAP program in an image function
    final ImageFunction<T> function =
      new JgapArithmeticImageFunction<>(individual.getGenotype());

    // Create a new individual having an image function as its genotype
    final Individual<ImageFunction<T>, BufferedImage, Object> individualFunction =
      new Individual<>(function);

    // Create the phenotype for the new individual using the internal
    // function creator
    this.getImageCreator().createForIndividual(individualFunction);

    // And copy over the created phenotype to our initial individual
    individual.setPhenotype(individualFunction.getPhenotype());
  }
}
