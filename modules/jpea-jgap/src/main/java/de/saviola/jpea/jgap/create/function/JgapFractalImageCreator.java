package de.saviola.jpea.jgap.create.function;

import java.awt.image.BufferedImage;

import org.apache.commons.math3.complex.Complex;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.create.function.FractalFunctionImageCreator;
import de.saviola.jpea.core.create.function.FractalImageFunction;

/**
 * Fractal image creator using {@link FractalImageFunction}s.
 */
public class JgapFractalImageCreator extends
  JgapImageCreator<FractalImageFunction<Complex>>
{
  public JgapFractalImageCreator(final int width, final int height)
  {
    super(new FractalFunctionImageCreator(width, height));
  }

  /**
   * TODO extract common logic from this class and
   * {@link JgapArithmeticImageCreator}.
   */
  @Override
  public void createForIndividual(
    final Individual<IGPProgram, BufferedImage, ?> individual)
  {
    // Bail out if the phenotype has already been created
    if (individual.getPhenotype() != null)
    {
      return;
    }

    // Wrap the JGAP program in an image function
    final FractalImageFunction<Complex> function =
      new JgapFractalImageFunction(individual.getGenotype());

    // Create a new individual having an image function as its genotype
    final Individual<FractalImageFunction<Complex>, BufferedImage, Object> individualFunction =
      new Individual<>(function);

    // Create the phenotype for the new individual using the internal
    // function creator
    this.getImageCreator().createForIndividual(individualFunction);

    // And copy over the created phenotype to our initial individual
    individual.setPhenotype(individualFunction.getPhenotype());
  }
}
