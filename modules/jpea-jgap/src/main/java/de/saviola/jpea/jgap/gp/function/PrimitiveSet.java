package de.saviola.jpea.jgap.gp.function;

import java.util.Collection;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.impl.GPConfiguration;

/**
 * Interface for primitive sets.
 */
public interface PrimitiveSet
{
  /**
   * Creates the primitive set.
   */
  public Collection<CommandGene> create(GPConfiguration conf)
    throws InvalidConfigurationException;
}
