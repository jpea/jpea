package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.evaluate.FitnessTransformation;
import de.saviola.jpea.core.evaluate.individual.RalphEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.evaluate.population.WeightedProductPopulationEvaluator;
import de.saviola.jpea.jgap.evaluate.individual.NodeCountEvaluator;

/**
 * Ralph scenario using bloat control.
 */
public class RalphBCScenario extends RalphScenario
{
  @Override
  public PopulationEvaluator<IGPProgram, BufferedImage, Double> createPopulationEvaluator()
    throws Exception
  {
    // Create the evaluator(s)
    final WeightedProductPopulationEvaluator<IGPProgram, BufferedImage> evaluator =
      new WeightedProductPopulationEvaluator<>();
    evaluator.addIndividualEvaluator(
      new RalphEvaluator<IGPProgram>(),
      new FitnessTransformation());
    evaluator.addIndividualEvaluator(
      new NodeCountEvaluator<BufferedImage>(), new FitnessTransformation(-1,
        .001, 1.));

    return super.createEvaluatorGui(evaluator);
  }

  @Override
  public String getTitle()
  {
    return "Ralph aestehtic measure (bloat control)";
  }
}
