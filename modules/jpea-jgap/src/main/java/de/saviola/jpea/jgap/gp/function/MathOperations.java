package de.saviola.jpea.jgap.gp.function;

import java.math.RoundingMode;
import java.util.Vector;

import de.saviola.jpea.jgap.gp.RNG;

/**
 * GP function set extension.
 * 
 * Contains three conditional functions ({@link #ifThenElse()},
 * {@link #greaterThan()} and {@link #lowerThan()}), the function for creating
 * the color vector and a number of conversion functions.
 */
public class MathOperations
{
  public static double add(final double a, final double b)
  {
    return a + b;
  }

  public static double booleanToDouble(final boolean b)
  {
    return b ? 1. : -1.;
  }

  public static Number booleanToNum(final boolean b)
  {
    return new Double(booleanToDouble(b));
  }

  public static Vector<Double> createColorVector(final double c1,
    final double c2,
    final double c3)
  {
    final Vector<Double> vector = new Vector<>();

    vector.add(new Double(c1));
    vector.add(new Double(c2));
    vector.add(new Double(c3));

    return vector;
  }

  public static double div(final double a, final double b)
  {
    return b != 0 ? a / b : 0;
  }

  public static Number doubleToNum(final Double n)
  {
    return n;
  }

  public static Number floatToNum(final Float n)
  {
    return n;
  }

  public static double greaterThan(final double d1, final double d2,
    final double d3, final double d4)
  {
    return d1 > d2 ? d3 : d4;
  }

  public static double ifThenElse(final double cond, final double then,
    final double els)
  {
    if (cond > 0)
    {
      return then;
    }

    return els;
  }

  public static Number intToNum(final Integer n)
  {
    return n;
  }

  public static Number longToNum(final Long n)
  {
    return n;
  }

  public static double lowerThan(final double d1, final double d2,
    final double d3, final double d4)
  {
    return d1 < d2 ? d3 : d4;
  }

  public static double mul(final double a, final double b)
  {
    return a * b;
  }

  public static double multiplicativeInverse(final double d1)
  {
    return d1 == 0 ? 0 : 1 / d1;
  }

  public static double negate(final double d1)
  {
    return -1 * d1;
  }

  public static boolean numToBoolean(final Number n)
  {
    return n.doubleValue() >= 0;
  }

  public static double numToDouble(final Number n)
  {
    return n.doubleValue();
  }

  public static float numToFloat(final Number n)
  {
    return n.floatValue();
  }

  public static int numToInt(final Number n)
  {
    return n.intValue();
  }

  public static long numToLong(final Number n)
  {
    return n.longValue();
  }

  public static RoundingMode randomRoundingMode()
  {
    RoundingMode rMode;

    switch (RNG.getRNG().nextInt(7))
    {
      case 0:
        rMode = RoundingMode.CEILING;
      break;
      case 1:
        rMode = RoundingMode.DOWN;
      break;
      case 2:
        rMode = RoundingMode.FLOOR;
      break;
      case 3:
        rMode = RoundingMode.HALF_DOWN;
      break;
      case 4:
        rMode = RoundingMode.HALF_EVEN;
      break;
      case 5:
        rMode = RoundingMode.HALF_UP;
      break;
      default:
        rMode = RoundingMode.UP;
      break;
    }

    return rMode;
  }

  public static double sub(final double a, final double b)
  {
    return a - b;
  }

  public static boolean terminalBoolean()
  {
    return RNG.getRNG().nextBoolean();
  }

  public static double terminalDouble()
  {
    return RNG.getRNG().nextDouble();
  }

  public static float terminalFloat()
  {
    return RNG.getRNG().nextFloat();
  }

  public static int terminalInt()
  {
    // TODO insert ceiling?
    return RNG.getRNG().nextInt();
  }

  public static long terminalLong()
  {
    return RNG.getRNG().nextLong();
  }
}