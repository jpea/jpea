package de.saviola.jpea.jgap.create.function;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.jgap.gp.function.Variable;

/**
 * Image function which executes the given {@link IGPProgram} instance
 * upon evaluation.
 * 
 * TODO check if there's only one instance of each variable an if so, store
 * it in the configuration as before
 */
public class JgapArithmeticImageFunction<T> extends JgapImageFunction<T>
{
  /**
   * Identifiers for the variables in the {@link IGPProgram} instances.
   * 
   * @see #setProgram(IGPProgram)
   */
  public static final String X_IDENTIFIER = "X", Y_IDENTIFIER = "Y";

  /**
   * The variables which will be set to the values provided to
   * {@link #evaluateAt(double, double)}.
   * 
   * TODO make static?
   * 
   * @see #evaluateAt(double, double)
   */
  private final Variable x;
  private final Variable y;

  /**
   * Takes the program which will be executed.
   */
  public JgapArithmeticImageFunction(final IGPProgram program)
  {
    super(program);

    this.x = (Variable) program.getGPConfiguration().readFromMemory("X");
    this.y = (Variable) program.getGPConfiguration().readFromMemory("Y");
  }

  /**
   * Executes the underlying {@link IGPProgram} instance after setting the
   * variables to the given values.
   */
  @Override
  public T evaluateAt(final double x, final double y)
  {
    this.x.setValue(new Double(x));
    this.y.setValue(new Double(y));

    // TODO better way to handle this?
    @SuppressWarnings("unchecked")
    final T result = (T) this.program.execute_object(0, noArgs);

    return result;
  }
}