package de.saviola.jpea.jgap.create.function;

import org.apache.commons.math3.complex.Complex;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.function.FractalImageFunction;
import de.saviola.jpea.jgap.gp.function.fractal.FractalVariables;

/**
 * Fractal image function, providing background color, starting position and
 * zoom for the fractal image.
 */
public class JgapFractalImageFunction extends
  JgapImageFunction<Complex> implements FractalImageFunction<Complex>
{

  public JgapFractalImageFunction(final IGPProgram program)
  {
    super(program);
  }

  @Override
  public Complex evaluateAt(final double x, final double y)
  {
    FractalVariables.setZOld(new Complex(1.));
    FractalVariables.setZ(new Complex(x, y));

    return (Complex) this.program.execute_object(0, noArgs);
  }

  @Override
  public double getBackgroundValue()
  {
    return ((Double) this.program.execute_object(3, noArgs)).doubleValue();
  }

  @Override
  public Complex getStartingPoint()
  {
    return new Complex(
      ((Double) this.program.execute_object(1, noArgs)).doubleValue(),
      ((Double) this.program.execute_object(2, noArgs)).doubleValue());
  }

  @Override
  public double getStepSize()
  {
    return ((Double) this.program.execute_object(4, noArgs)).doubleValue();
  }

}
