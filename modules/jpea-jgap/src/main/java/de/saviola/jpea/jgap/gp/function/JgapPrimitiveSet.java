package de.saviola.jpea.jgap.gp.function;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.regep.core.filter.BlacklistFilter;
import de.saviola.regep.core.filter.interfaces.FilterChain;

/**
 * Abstract Jgap primitive set to exclude random mehtods.
 */
public abstract class JgapPrimitiveSet implements PrimitiveSet
{
  private final static Logger logger = LoggerFactory
    .getLogger(JgapPrimitiveSet.class);

  protected void appendRandomMethodFilter(final FilterChain filterChain)
  {
    Method randomMethod;
    try
    {
      randomMethod = Math.class.getMethod("random");
      final BlacklistFilter<Method> filter =
        new BlacklistFilter<>(new Method[] { randomMethod });
      filterChain.addFilter(filter);
    }
    catch (
      NoSuchMethodException | SecurityException e1)
    {
      logger.warn("Blacklist filter for Math.random method could not be "
        + "created, seeding will not be possible.");
      e1.printStackTrace();
    }
  }

}
