package de.saviola.jpea.jgap.evaluate;

import java.util.Comparator;

import org.jgap.gp.impl.DeltaGPFitnessEvaluator;

import de.saviola.jpea.core.evaluate.JpeaFitnessEvaluator;

/**
 * Fitness evaluator extending the JGAP fitness evaluator
 * {@link DeltaGPFitnessEvaluator} to be used in JGAP.
 */
public class JgapDoubleFitnessEvaluator extends DeltaGPFitnessEvaluator
{
  private static final long serialVersionUID = -8790058168606027575L;

  private final JpeaFitnessEvaluator<Double> fitnessEvaluator;

  public JgapDoubleFitnessEvaluator(
    final Comparator<Double> comparator)
  {
    this.fitnessEvaluator =
      new JpeaFitnessEvaluator<>(comparator);
  }

  @Override
  public boolean isFitter(final double a_fitness_value1,
    final double a_fitness_value2)
  {
    return this.fitnessEvaluator.isFitter(new Double(a_fitness_value1),
      new Double(a_fitness_value2));
  }
}
