package de.saviola.jpea.jgap;

import java.util.Arrays;

import org.jgap.gp.GPFitnessFunction;
import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.impl.DefaultGPFitnessEvaluator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.Population;
import de.saviola.jpea.core.application.EvolutionEngine;
import de.saviola.jpea.core.application.FrameworkDriver;
import de.saviola.jpea.core.exception.EndOfEvolutionException;

/**
 * JGAP framework driver.
 * 
 * @param <P> Phenotype of the individuals
 */
@SuppressWarnings("serial")
public class JgapDriver<P> extends GPFitnessFunction implements
  FrameworkDriver<IGPProgram, P, Double>
{
  private final static Logger logger = LoggerFactory
    .getLogger(JgapDriver.class);

  /**
   * This is the actual framework configuration object.
   */
  private GPConfiguration config;

  /**
   * The evolution engine working as a kind of callback.
   */
  private EvolutionEngine<IGPProgram, P, Double> evolutionEngine;

  /**
   * Current generation number.
   */
  private int generation = -1;

  /**
   * Initialization flag.
   * 
   * TODO needed?
   */
  private boolean initialized = false;

  /**
   * JGAP {@link GPGenotype}, needed to start the evolution.
   */
  private GPGenotype jgapGenotype;

  /**
   * The current generation's population.
   */
  private Population<IGPProgram, P, Double> population;

  /**
   * GP problem.
   */
  private GPProblem problem;

  /**
   * Evaluates the given {@link IGPProgram}.
   */
  @Override
  protected double evaluate(final IGPProgram genotype)
  {
    // Set the current generation number
    final int generation = genotype.getGPConfiguration().getGenerationNr();

    // If we have a new generation, we have a new population as well
    if (generation != this.generation)
    {
      this.processPopulation();

      if (this.generation >= 0)
      {
        logger.debug("Finished evolution of generation {}",
          new Integer(this.generation));
      }

      this.generation = generation;
    }

    // Retrieve the current individual from our population
    final Individual<IGPProgram, P, Double> individual =
      this.population.getIndividualByGenotype(genotype);

    if (!individual.hasEvaluation())
    {
      throw new RuntimeException("Error in evaluator: Missing individual " +
        "evaluation.");
    }

    return individual.getEvaluation().doubleValue();
  }

  /**
   * Returns the JGAP configuration object.
   */
  public GPConfiguration getConfig()
  {
    return this.config;
  }

  /**
   * Returns the used {@link EvolutionEngine}.
   */
  public EvolutionEngine<IGPProgram, P, Double> getEvolutionEngine()
  {
    return this.evolutionEngine;
  }

  /**
   * Returns the JGAP {@link GPGenotype}.
   */
  public GPGenotype getJgapGenotype()
  {
    return this.jgapGenotype;
  }

  public GPProblem getProblem()
  {
    return this.problem;
  }

  /**
   * Initializes the driver, setting up some default configuration values.
   */
  @Override
  public void initialize() throws Exception
  {
    this.config = new GPConfiguration();
    this.config.setGPFitnessEvaluator(new DefaultGPFitnessEvaluator());
    this.config.setMaxInitDepth(4);
    this.config.setPopulationSize(50);
    this.config.setStrictProgramCreation(false);
    this.config.setFitnessFunction(this);

    this.initialized = true;
  }

  /**
   * Returns whether this driver is initialized.
   */
  public boolean isInitialized()
  {
    return this.initialized;
  }

  /**
   * Processes the current population of individuals.
   */
  private void processPopulation()
  {
    // Create new population
    this.population = new Population<>(
      Arrays.asList(this.jgapGenotype.getGPPopulation().getGPPrograms()));

    try
    {
      // Creates phenotypes for and evaluates the whole population
      this.evolutionEngine.processPopulation(this.population);
    }
    catch (final EndOfEvolutionException eoe)
    {
      throw eoe;
    }
    // TODO what do we expect to catch?
    catch (final Exception e)
    {
      logger.warn("Exception caught while evaluating population.");

      e.printStackTrace();
    }
  }

  /**
   * Set the GP problem.
   */
  public void setProblem(final GPProblem problem)
  {
    this.problem = problem;
  }

  /**
   * Starts the JGAP evolution.
   */
  @Override
  public void startEvolution(final EvolutionEngine<IGPProgram, P, Double> engine)
    throws Exception
  {
    // Abort if the driver was not initialized
    if (!this.isInitialized())
    {
      throw new RuntimeException("Uninitialized driver can't be started.");
    }
    else if (this.problem == null)
    {
      throw new RuntimeException("No GPProblem has been set.");
    }

    // Create GPGenotype, set it up and store its reference
    final GPGenotype gp = this.problem.create();
    gp.setVerboseOutput(true);
    this.jgapGenotype = gp;

    this.evolutionEngine = engine;

    // Read the number of generations from the configuration
    // TODO make configurable in GUI
    Integer numGenerations;
    try
    {
      numGenerations = (Integer) this.getConfig().readFromMemory("generations");
    }
    catch (final IllegalArgumentException e)
    {
      numGenerations = new Integer(1000);
    }

    // Start the actual evolution
    // TODO make configurable
    this.jgapGenotype.evolve(numGenerations == null ? 1000
      : numGenerations.intValue());
  }
}
