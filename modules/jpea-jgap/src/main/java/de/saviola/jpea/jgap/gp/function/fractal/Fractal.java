package de.saviola.jpea.jgap.gp.function.fractal;

import org.apache.commons.math3.complex.Complex;

/**
 * Helper fractal class.
 */
public class Fractal extends Complex
{
  private static final long serialVersionUID = 8396766080090074917L;

  public Fractal(final Complex c)
  {
    super(c.getReal(), c.getImaginary());
  }

  public Fractal(final double real)
  {
    super(real);
  }

  public Fractal(final double real, final double imaginary)
  {
    super(real, imaginary);
  }
}
