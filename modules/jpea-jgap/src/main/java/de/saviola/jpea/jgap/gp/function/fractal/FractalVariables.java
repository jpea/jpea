package de.saviola.jpea.jgap.gp.function.fractal;

import org.apache.commons.math3.complex.Complex;

/**
 * Currently not used.
 */
public class FractalVariables
{
  private static ThreadLocal<Complex> z = new ThreadLocal<Complex>()
  {
    @Override
    protected Complex initialValue()
    {
      return new Complex(0.);
    }
  };

  private static ThreadLocal<Complex> zOld = new ThreadLocal<Complex>()
  {
    @Override
    protected Complex initialValue()
    {
      return new Complex(0.);
    }
  };

  public static Complex getZ()
  {
    return z.get();
  }

  public static Complex getZOld()
  {
    return zOld.get();
  }

  public static void setZ(final Complex c)
  {
    z.set(c);
  }

  public static void setZOld(final Complex c)
  {
    zOld.set(c);
  }
}
