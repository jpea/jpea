package de.saviola.jpea.jgap.gp.function;

import java.awt.image.BufferedImage;
import java.util.Collection;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.impl.GPConfiguration;

import com.google.common.math.DoubleMath;

import de.saviola.jpea.jgap.gp.function.fractal.ComplexFunctions;
import de.saviola.regep.core.processing.ConcurrentRecursiveFilteringProcessorChain;
import de.saviola.regep.core.processing.reflect.ClassToMethodsProcessor;
import de.saviola.regep.core.processing.reflect.StaticMethodProcessor;
import de.saviola.regep.jgap.processing.CommandGeneProcessor;
import de.saviola.regep.jgap.processing.FunctionProcessor;

/**
 * Fractal primitive set.
 */
public class FractalPrimitiveSet extends JgapPrimitiveSet
{

  @Override
  public Collection<CommandGene> create(final GPConfiguration conf)
    throws InvalidConfigurationException
  {
    // Create processing structure
    final ConcurrentRecursiveFilteringProcessorChain processorChain =
      new ConcurrentRecursiveFilteringProcessorChain(10);

    this.appendRandomMethodFilter(processorChain);

    //processorChain.addProcessor(new ClassToConstructorsProcessor());
    processorChain.addProcessor(new ClassToMethodsProcessor());
    //processorChain.addProcessor(new ConstructorProcessor());
    //processorChain.addProcessor(new InstanceMethodProcessor());
    processorChain.addProcessor(new StaticMethodProcessor());
    processorChain
      .addProcessor(new FunctionProcessor(conf));
    final CommandGeneProcessor commandGeneProcessor =
      new CommandGeneProcessor();
    processorChain.addProcessor(commandGeneProcessor);
    commandGeneProcessor.getFunctionDatabase().addRootType(BufferedImage.class);

    // Process classes
    try
    {
      //processorChain.process("de.saviola.jpea.jgap.gp.function");
      processorChain.process(ComplexFunctions.class);
      //processorChain.process(FractalVariables.class);
      processorChain.process(MathOperations.class);
      //processorChain.process(FractalOperations.class);
      processorChain.process(Math.class);
      //processorChain.process(IntMath.class);
      //processorChain.process(LongMath.class);
      processorChain.process(DoubleMath.class);
    }
    catch (final Exception e)
    {
      // TODO handle exception
      e.printStackTrace();
    }

    // Get the function set
    final Collection<CommandGene> functionSet =
      commandGeneProcessor.getCommandGenes();

    return functionSet;
  }

}
