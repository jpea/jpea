package de.saviola.jpea.jgap.evaluate.individual;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.individual.AbstractIndividualEvaluator;
import de.saviola.jpea.core.evaluate.individual.compare.DynamicDoubleComparator;

/**
 * Simple node count evaluator.
 */
public class NodeCountEvaluator<P> extends
  AbstractIndividualEvaluator<IGPProgram, P, Double>
{
  public NodeCountEvaluator()
  {
    this.setComparator(new DynamicDoubleComparator<>(this));
  }

  /**
   * TODO work with tree depth instead of node count?
   */
  @Override
  public Double evaluateIndividual(
    final Individual<IGPProgram, P, Double> individual)
  {
    final IGPProgram genotype = individual.getGenotype();
    final int genotypeSize = genotype.size();

    int nodeCount = 0;
    for (int i = 0; i < genotypeSize; i++)
    {
      nodeCount += genotype.getChromosome(i).size();
    }

    return new Double(nodeCount);
  }

  @Override
  public Double getBestEvaluation()
  {
    return new Double(0.);
  }

  @Override
  public Double getWorstEvaluation()
  {
    return new Double(Double.POSITIVE_INFINITY);
  }
}
