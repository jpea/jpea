package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.core.create.function.VectorFunctionImageCreator;
import de.saviola.jpea.core.evaluate.gui.InteractiveImageEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.jgap.application.config.JgapDriverConfigurator;
import de.saviola.jpea.jgap.create.function.JgapArithmeticImageCreator;
import de.saviola.jpea.jgap.evaluate.gui.GenotypeViewer;
import de.saviola.jpea.jgap.gp.problem.SimpleArithmeticProblem;

/**
 * Interactive scenario.
 * 
 * TODO use abstract scenario
 */
public class InteractiveScenario extends JgapScenario<BufferedImage>
{
  /**
   * The GUI.
   * 
   * TODO implement this differently, somehow?
   */
  private static InteractiveImageEvaluator<IGPProgram> gui;

  @Override
  public PhenotypeCreator<IGPProgram, BufferedImage> createPhenotypeCreator()
  {
    // TODO Make image size and image creator configurable?
    return new JgapArithmeticImageCreator<>(new VectorFunctionImageCreator(500,
      500));
  }

  @Override
  public PopulationEvaluator<IGPProgram, BufferedImage, Double> createPopulationEvaluator()
    throws Exception
  {
    // Set up evaluator GUI
    SwingUtilities.invokeAndWait(new Runnable()
    {
      @Override
      public void run()
      {
        gui =
          new InteractiveImageEvaluator<>();

        gui.addIndividualViewer(new GenotypeViewer<BufferedImage, Double>());
      }
    });

    return InteractiveScenario.gui;
  }

  @Override
  public GPProblem createProblem() throws Exception
  {
    return new SimpleArithmeticProblem(this.getDriver().getConfig());
  }

  @Override
  public String getDescription()
  {
    return this.getTitle();
  }

  @Override
  public String getTitle()
  {
    return "Interactive image evolution";
  }

  @Override
  public void setUpConfigurators() throws Exception
  {
    // Driver configurator
    final JgapDriverConfigurator jgapDriverConfigurator =
      new JgapDriverConfigurator(this.getDriver());
    jgapDriverConfigurator.enableFunctionProbability(50, 90, 90);
    jgapDriverConfigurator.enableNewIndividualsPercent(10, 50, 30);
    jgapDriverConfigurator.enableTreeDepthMaximum(4, 20, 7);
    jgapDriverConfigurator.enableTreeDepthMinimum(2, 10, 2);

    // Set population size
    this.getDriver().getConfig().setPopulationSize(20);

    // Add configurator
    this.getConfigurator().addConfigurator(jgapDriverConfigurator);

  }
}
