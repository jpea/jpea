package de.saviola.jpea.jgap.gp.function;

import java.util.Collection;
import java.util.Vector;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.impl.GPConfiguration;

import com.google.common.math.DoubleMath;
import com.google.common.math.IntMath;
import com.google.common.math.LongMath;

import de.saviola.regep.core.processing.ConcurrentRecursiveFilteringProcessorChain;
import de.saviola.regep.core.processing.reflect.ClassToMethodsProcessor;
import de.saviola.regep.core.processing.reflect.StaticMethodProcessor;
import de.saviola.regep.jgap.processing.CommandGeneProcessor;
import de.saviola.regep.jgap.processing.FunctionProcessor;

/**
 * Simple arithmetic primitive set.
 */
public class SimpleArithmeticPrimitiveSet extends JgapPrimitiveSet
{
  @Override
  public Collection<CommandGene> create(final GPConfiguration conf)
    throws InvalidConfigurationException
  {
    // Create processing structure
    final ConcurrentRecursiveFilteringProcessorChain processorChain =
      new ConcurrentRecursiveFilteringProcessorChain(10);

    this.appendRandomMethodFilter(processorChain);

    //processorChain.addProcessor(new ClassToConstructorsProcessor());
    processorChain.addProcessor(new ClassToMethodsProcessor());
    //processorChain.addProcessor(new ConstructorProcessor());
    //processorChain.addProcessor(new InstanceMethodProcessor());
    processorChain.addProcessor(new StaticMethodProcessor());
    processorChain
      .addProcessor(new FunctionProcessor(conf));
    final CommandGeneProcessor commandGeneProcessor =
      new CommandGeneProcessor();
    processorChain.addProcessor(commandGeneProcessor);
    commandGeneProcessor.getFunctionDatabase().addRootType(Vector.class);

    // Process classes
    try
    {
      processorChain.process(MathOperations.class);
      //processorChain.process(FractalOperations.class);
      processorChain.process(Math.class);
      processorChain.process(IntMath.class);
      processorChain.process(LongMath.class);
      processorChain.process(DoubleMath.class);
    }
    catch (final Exception e)
    {
      // TODO handle exception
      e.printStackTrace();
    }

    // Get the function set
    final Collection<CommandGene> functionSet =
      commandGeneProcessor.getCommandGenes();

    // Create variables and store them for later use
    conf.storeInMemory("X",
      new Variable(conf, "X", Double.class));
    conf.storeInMemory("Y",
      new Variable(conf, "Y", Double.class));

    // Add variables
    functionSet.add((CommandGene) conf.readFromMemory(
      "X"));
    functionSet.add((CommandGene) conf.readFromMemory(
      "Y"));

    return functionSet;
  }

}
