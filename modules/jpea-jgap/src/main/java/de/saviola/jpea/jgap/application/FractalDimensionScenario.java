package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.evaluate.FitnessTransformation;
import de.saviola.jpea.core.evaluate.individual.FractalDimensionEvaluator;
import de.saviola.jpea.core.evaluate.population.PopulationEvaluator;
import de.saviola.jpea.core.evaluate.population.WeightedProductPopulationEvaluator;

/**
 * Fractal dimension scenario.
 */
public class FractalDimensionScenario extends AutomaticScenario
{

  @Override
  public PopulationEvaluator<IGPProgram, BufferedImage, Double> createPopulationEvaluator()
    throws Exception
  {
    // Create the evaluator(s)
    final WeightedProductPopulationEvaluator<IGPProgram, BufferedImage> evaluator =
      new WeightedProductPopulationEvaluator<>();
    evaluator.addIndividualEvaluator(
      new FractalDimensionEvaluator<IGPProgram>(),
      new FitnessTransformation());

    return super.createEvaluatorGui(evaluator);
  }

  @Override
  public String getTitle()
  {
    return "Fractal dimension";
  }
}
