package de.saviola.jpea.jgap.gp.problem;

import java.awt.image.BufferedImage;
import java.util.Collection;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.GPProblem;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;

import de.saviola.jpea.jgap.gp.function.FractalPrimitiveSet;

/**
 * Fractal problem.
 */
public class FractalProblem extends GPProblem
{
  public FractalProblem(final GPConfiguration config)
    throws InvalidConfigurationException
  {
    super(config);
  }

  /**
   * TODO extract common logic from this class and {@link AdfArithmeticProblem}.
   */
  @Override
  public GPGenotype create() throws InvalidConfigurationException
  {
    final Collection<CommandGene> functionSet =
      new FractalPrimitiveSet().create(this.getGPConfiguration());

    final Class<?>[] types = {
      BufferedImage.class };
    final Class<?>[][] argTypes = {
      {},
    };

    final CommandGene[] functions = functionSet.toArray(new CommandGene[] {});

    // Define the commands and terminals the GP is allowed to use.
    final CommandGene[][] nodeSets = {
      functions,
    };

    // Create genotype with initial population.
    // Allow max. 500 nodes within one program.
    // TODO make max node number configurable
    return GPGenotype.randomInitialGenotype(this.getGPConfiguration(), types,
      argTypes, nodeSets,
      500, true);
  }

}
