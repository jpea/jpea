package de.saviola.jpea.jgap.application;

import java.awt.image.BufferedImage;

import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.jgap.create.JgapPhenotypeCreator;
import de.saviola.jpea.jgap.gp.problem.FractalProblem;

/**
 * Fractal scenario.
 */
public class FractalScenario extends GcfScenario
{
  @Override
  public PhenotypeCreator<IGPProgram, BufferedImage> createPhenotypeCreator()
    throws Exception
  {
    return new JgapPhenotypeCreator<>();
  }

  @Override
  public GPProblem createProblem() throws Exception
  {
    return new FractalProblem(this.getDriver().getConfig());
  }

  @Override
  public String getTitle()
  {
    return "Fractal image creation";
  }
}
