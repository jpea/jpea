package de.saviola.jpea.jgap.gp.function.fractal;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;

import org.apache.commons.math3.complex.Complex;
import org.imgscalr.Scalr;

/**
 * Class of static wrappers for the functions of the {@link Complex} class.
 */
public class ComplexFunctions
{
  public static Complex acos(final Complex c1)
  {
    return c1.acos();
  }

  public static Complex add(final Complex c1, final Complex c2)
  {
    return c1.add(c2);
  }

  public static Complex add(final Complex c1, final double d1)
  {
    return c1.add(d1);
  }

  public static double add(final double d1, final double d2)
  {
    return d1 + d2;
  }

  public static Complex asin(final Complex c1)
  {
    return c1.asin();
  }

  public static Complex atan(final Complex c1)
  {
    return c1.atan();
  }

  public static Complex conjugate(final Complex c1)
  {
    return c1.conjugate();
  }

  public static Complex cos(final Complex c1)
  {
    return c1.cos();
  }

  public static Complex cosh(final Complex c1)
  {
    return c1.cosh();
  }

  public static BufferedImage createFractalImage(final double exponent,
    double iterations, final Complex c, final Complex stepSize,
    final double backgroundColor)
  {
    final int height = 500, width = 500;

    //iterations = (int) ((iterations + 1) * 25);
    iterations = 10;

    while (iterations > 50)
    {
      iterations /= 2;
    }

    if (iterations < 5)
    {
      iterations = 10;
    }

    final int[] colorArray = new int[height * width];
    final int backgroundColorInt =
      0xFF000000 + (int) (0xFFFFFF * ((backgroundColor + 1) / 2.));
    Complex z;
    Complex cTmp = new Complex(c.getReal(), c.getImaginary());
    final Complex stepX = new Complex(stepSize.getReal()), stepY =
      new Complex(0., stepSize.getImaginary());
    double lyapunov;

    for (int x = 0; x < width; x++)
    {
      for (int y = 0; y < height; y++)
      {
        z = cTmp;

        for (int i = 0; i < iterations; i++)
        {
          z = z.pow(exponent).add(cTmp);
        }

        lyapunov = 1. / iterations * Math.log(z.abs());

        colorArray[x * height + y] = lyapunov < 0 ? backgroundColorInt :
          0xFF000000 + ((int) (0xFFFFFF * (1. / lyapunov)) & 0x00FFFFFF);

        cTmp = cTmp.add(stepY);
      }

      cTmp = new Complex(cTmp.getReal(), c.getImaginary()).add(stepX);
    }

    final int[] bitMasks = new int[] { 0xFF0000, 0xFF00, 0xFF, 0xFF000000 };
    final SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(
      DataBuffer.TYPE_INT, width, height, bitMasks);
    final DataBufferInt db = new DataBufferInt(colorArray, colorArray.length);
    final WritableRaster wr =
      Raster.createWritableRaster(sm, db, new Point());
    final BufferedImage image =
      new BufferedImage(ColorModel.getRGBdefault(), wr, true, null);

    return Scalr.resize(image, 500);
  }

  public static Complex div(final Complex c1, final Complex c2)
  {
    return c1.divide(c2);
  }

  public static Complex divDouble(final Complex c1, final double d1)
  {
    return c1.divide(d1);
  }

  public static Complex exp(final Complex c1)
  {
    return c1.exp();
  }

  public static Complex log(final Complex c1)
  {
    return c1.log();
  }

  public static Complex mul(final Complex c1, final Complex c2)
  {
    return c1.multiply(c2);
  }

  public static double mul(final double d1, final double d2)
  {
    return d1 * d2;
  }

  public static Complex mulDouble(final Complex c1, final double d1)
  {
    return c1.multiply(d1);
  }

  public static double neg(final double d1)
  {
    return -d1;
  }

  public static Complex negate(final Complex c1)
  {
    return c1.negate();
  }

  public static Complex pow(final Complex c1, final Complex c2)
  {
    return c1.pow(c2);
  }

  public static Complex powDouble(final Complex c1, final double d1)
  {
    return c1.pow(d1);
  }

  public static Complex randomComplex()
  {
    return new Complex(Math.random(), Math.random());
  }

  public static double rec(final double d1)
  {
    return d1 == 0 ? 0. : 1 / d1;
  }

  public static Complex reciprocal(final Complex c1)
  {
    return c1.reciprocal();
  }

  public static Complex sin(final Complex c1)
  {
    return c1.sin();
  }

  public static Complex sinh(final Complex c1)
  {
    return c1.sinh();
  }

  public static Complex sqrt(final Complex c1)
  {
    return c1.sqrt();
  }

  public static Complex sqrt1z(final Complex c1)
  {
    return c1.sqrt1z();
  }

  public static Complex tan(final Complex c1)
  {
    return c1.tan();
  }

  public static Complex tanh(final Complex c1)
  {
    return c1.tanh();
  }

  public static double terminal()
  {
    return Math.random();
  }
}
