package de.saviola.jpea.jgap.evaluate.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import org.jgap.gp.CommandGene;
import org.jgap.gp.IGPChromosome;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.impl.ProgramChromosome;

import de.saviola.jpea.core.Individual;
import de.saviola.jpea.core.evaluate.gui.individual.DisplayableIndividualViewer;

/**
 * Displays the genotype of individuals in a directory-styled tree.
 */
public class GenotypeViewer<P, V> implements
  DisplayableIndividualViewer<IGPProgram, P, V>
{
  /**
   * Panel which will hold the gentoype.
   */
  private final JPanel panel;

  /**
   * {@link JTree} representing the genotype.
   */
  private JTree tree;

  /**
   * Label showing the genotype statistics.
   */
  private final JLabel labelStats;

  /**
   * Patterns for parsing the genotype string of JGAP.
   */
  private static String packagePattern = "(?:\\w+\\.)*";
  private static String classPattern = packagePattern + "\\w+";
  private static String returnValuePattern = "\\[class (" + classPattern
    + ")\\]";
  private static String methodQualifiers = "(?:\\w+ )*";
  private static String methodPattern = methodQualifiers +
    "(?:" + classPattern + " )?" + packagePattern + "\\w+\\.(\\w+)" +
    "\\((?:" + classPattern + ")?(?:," + classPattern + ")*\\)";
  private static Pattern geneNamePattern =
    Pattern.compile(returnValuePattern + methodPattern);

  public GenotypeViewer()
  {
    this.panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
    this.panel.setBorder(BorderFactory.createTitledBorder("Genotype viewer"));
    this.panel.setPreferredSize(new Dimension(500, 500));

    this.labelStats = new JLabel("Genotype depth: - | node count: -");
    this.panel.add(this.labelStats);
  }

  /**
   * Clean the gene name.
   * 
   * Removes the argument information of the gene.
   */
  private String cleanGeneName(final String gene)
  {
    final Matcher matcher = geneNamePattern.matcher(gene);

    return matcher.replaceFirst("$1 $2()");
  }

  /**
   * Recursively creates nodes of the tree.
   */
  private MutableTreeNode createNode(final int forIndex,
    final IGPChromosome ofChromosome)
  {
    final CommandGene gene = ofChromosome.getNode(forIndex);

    // Create the current root node
    final DefaultMutableTreeNode rootNode =
      new DefaultMutableTreeNode(this.cleanGeneName(gene.toString()), true);

    // Create the child nodes
    for (int i = 0; i < gene.getArity(null); i++)
    {
      rootNode.add(this.createNode(ofChromosome.getChild(forIndex, i),
        ofChromosome));
    }

    return rootNode;
  }

  /**
   * Returns the panel holding the genotype representation.
   */
  @Override
  public Component getComponent()
  {
    return this.panel;
  }

  /**
   * Show the given individual's genotype.
   */
  @Override
  public void showIndividual(final Individual<IGPProgram, P, V> individual)
  {
    // Remove the old tree
    if (this.tree != null)
    {
      this.panel.remove(this.tree);
    }

    final ProgramChromosome chromosome =
      individual.getGenotype().getChromosome(0);

    // Create the new tree
    this.tree = new JTree(this.createNode(0, chromosome));

    // Update the panel
    this.panel.add(this.tree);
    this.panel.revalidate();
    this.panel.repaint();

    this.labelStats.setText("Genotype depth: " + chromosome.getDepth(0) +
      " | node count: " + chromosome.size());
  }
}
