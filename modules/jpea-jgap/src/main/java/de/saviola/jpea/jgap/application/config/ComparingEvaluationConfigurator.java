package de.saviola.jpea.jgap.application.config;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import marvin.io.MarvinImageIO;

import org.imgscalr.Scalr;

import de.saviola.jpea.core.application.Configurator;

/**
 * Configurator for the comparing image evaluator.
 * 
 * Allows the target image to be chosen. Currently unused.
 */
@SuppressWarnings("serial")
public class ComparingEvaluationConfigurator extends JPanel implements
  Configurator, ActionListener
{
  /**
   * Button activating the {@link JFileChooser}.
   */
  private JButton buttonFileChooser;

  /**
   * The file chooser.
   */
  private JFileChooser fileChooser;

  /**
   * The target image.
   */
  private BufferedImage targetImage;

  /**
   * Label containing the file path of the target image.
   */
  private JLabel textFieldFileLocation;

  /**
   * Will be called when the file choosing button is clicked.
   */
  @Override
  public void actionPerformed(final ActionEvent event)
  {
    if (event.getSource().equals(this.buttonFileChooser))
    {
      // Let the user show a file
      final int result = this.fileChooser.showOpenDialog(this);

      // If the dialog was not aborted
      if (result == JFileChooser.APPROVE_OPTION)
      {
        final File targetImageFile = this.fileChooser.getSelectedFile();

        // Try to load the image
        try
        {
          this.targetImage =
            MarvinImageIO.loadImage(targetImageFile.getAbsolutePath())
              .getBufferedImage();

          String path = targetImageFile.getAbsolutePath();

          this.targetImage = Scalr.resize(this.targetImage, 500);

          // Shorten the path if necessary
          if (path.length() > 80)
          {
            path = path.substring(path.length() - 80);
          }

          // Update the label
          this.textFieldFileLocation.setText("Chosen image: " + path);
        }
        catch (final Exception e)
        {
          JOptionPane.showMessageDialog(this, "Invalid file chosen!");
        }
      }
    }

  }

  /**
   * Returns the target image or null of none was chosen yet.
   */
  public BufferedImage getTargetImage()
  {
    return this.targetImage;
  }

  /**
   * Initializes the configurator.
   */
  @Override
  public void initialize()
  {
    // Abort if this is not the EDT
    if (!SwingUtilities.isEventDispatchThread())
    {
      throw new RuntimeException("GUI modification must happen on the EDT!");
    }

    // Setup label
    this.textFieldFileLocation = new JLabel("No file chosen.");

    // Setup button
    this.buttonFileChooser = new JButton("Choose target image");
    this.buttonFileChooser.addActionListener(this);

    // Create file chooser
    this.fileChooser = new JFileChooser(new File("."));

    // Setup main panel
    this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    this.setBorder(BorderFactory.createTitledBorder
      ("Comparing image evaluation configuration"));
    this.add(this.textFieldFileLocation);
    this.add(this.buttonFileChooser);
  }

  /**
   * Throws an exception if no image was chosen.
   */
  @Override
  public void updateConfiguration() throws Exception
  {
    if (this.targetImage == null)
    {
      // TODO create ConfigurationException
      throw new Exception("No target image chosen.");
    }
  }
}
