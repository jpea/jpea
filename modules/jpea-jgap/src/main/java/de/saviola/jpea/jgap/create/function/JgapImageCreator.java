package de.saviola.jpea.jgap.create.function;

import java.awt.image.BufferedImage;

import org.jgap.gp.IGPProgram;

import de.saviola.jpea.core.create.ConcurrentPhenotypeCreator;
import de.saviola.jpea.core.create.PhenotypeCreator;
import de.saviola.jpea.core.create.function.FunctionImageCreator;
import de.saviola.jpea.core.create.function.VectorFunctionImageCreator;

/**
 * Image creator for JGAP programs (type {@link IGPProgram}) which return
 * a vector of numeric values.
 * 
 * It uses a {@link VectorFunctionImageCreator} to which it passes the JGAP
 * program – wrapped in a {@link JgapArithmeticImageFunction}.
 */
public abstract class JgapImageCreator<V>
  extends ConcurrentPhenotypeCreator<IGPProgram, BufferedImage>
{
  /**
   * The {@link FunctionImageCreator} which will be used to process the
   * {@link JgapArithmeticImageFunction} objects in which the JGAP programs will be
   * wrapped
   */
  private final PhenotypeCreator<V, BufferedImage> imageCreator;

  /**
   * Takes the width and height of the images to be created.
   * 
   * @see VectorFunctionImageCreator#VectorFunctionImageCreator(int, int)
   */
  public JgapImageCreator(
    final PhenotypeCreator<V, BufferedImage> imageCreator)
  {
    this.imageCreator = imageCreator;
  }

  public PhenotypeCreator<V, BufferedImage> getImageCreator()
  {
    return this.imageCreator;
  }

}