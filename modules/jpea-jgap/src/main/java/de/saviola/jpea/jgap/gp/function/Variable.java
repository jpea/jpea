package de.saviola.jpea.jgap.gp.function;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.ProgramChromosome;

/**
 * Variable implementation for use with JGAP.
 */
public class Variable extends CommandGene
{
  private static final long serialVersionUID = 7719640541034738142L;

  private final String identifier;

  private final ThreadLocal<Object> value = new ThreadLocal<Object>()
  {
    @Override
    protected Object initialValue()
    {
      return new Object();
    }
  };

  public Variable(final GPConfiguration a_conf, final String identifier,
    final Class<?> returnType)
    throws InvalidConfigurationException
  {
    super(a_conf, 0, returnType);

    this.identifier = identifier;
  }

  @Override
  public Object execute(final ProgramChromosome c, final int n,
    final Object[] args)
  {
    return this.execute_object(c, n, args);
  }

  @Override
  public Object execute_object(final ProgramChromosome c, final int n,
    final Object[] args)
  {
    return this.getValue();
  }

  public Object getValue()
  {
    return this.value.get();
  }

  public void setValue(final Object value)
  {
    this.value.set(value);
  }

  @Override
  public String toString()
  {
    return this.identifier;
  }
}
